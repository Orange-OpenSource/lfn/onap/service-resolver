swagger: "2.0"
info:
  version: "0.2.0_inProgress"
  title: "ProcessFlowManagement"
  description: |+
    API providing ProcessFlow & TaskFlow instances management capabilities from Camunda (TMF normalized)

    This API allows to:
    - Create a ProcessFlow (an instance of a processFlow type). 
    - Delete a ProcessFlow.
    - Get information about ProcessFlow and Task Flow execution in progress.
    - Modify a ProcessFlow/ Task Flow instance if needed.


    **ProcessFlow resource**
    A ProcessFlow resource allows to manage a ProcessFlow managed on Camunda. A ProcessFlow is triggered by an event and will manage a list of TaskFlow(s). 

    ProcessFlowManagement API performs the following operations on a ProcessFlow:
    - Retrieval of a ProcessFlow or a collection of ProcessFlow depending on filter criteria
    - Creation of a ProcessFlow instance(from a ProcessFlow type)
    - Deletion of a ProcessFlow instance 

    **TaskFlow resource**
    A taskFlow is one specific task of a process. A TaskFlow may need human intervention to be completed (data capture). TaskFlow is a ProcessFlow subResource (A taskFlow cannot be independant from a processFlow). A taskFlow is an instance of a processFlow type.

    ProcessFlowManagement API performs the following operations on a task Flow:
    - Retrieval of a TaskFlow or a collection of TaskFlow (by ProcessFlow ID)
    - Retrieval of a TaskFlow by ID
    - Partial update of a TaskFlow 
basePath: "/serviceResolver/ProcessFlowManagement/api/v1"
schemes:
- "http"
produces:
- "application/json"
tags:
- name: "ProcessFlow"
- name: "TaskFlow"

paths:

  /processFlow/{processFlowSpecification}{characteristic}:
    post:
      tags:
      - "ProcessFlow"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      operationId: "processflow_create"
      x-swagger-router-controller: "controllers.processflow_management_controllers"
      summary: "create processFlow"
      parameters:
      - name: "processFlowSpecification"
        required: true
        in: "path"
        type: "string"
      - name: "characteristic"
        required: true
        in: "path"
        type: "array"
        items: {}
      responses:
        "201":
          description: "Created"
          schema:
            $ref: "#/definitions/ProcessFlow"
        "400":
          description: "Could not create the ProcessFlow instance"
          schema:
            $ref: "#/definitions/Error"


  /processFlow/{processFlowId}:
    get:
      tags:
      - "ProcessFlow"
      produces:
      - "application/json"
      operationId: "processflow_get"
      x-swagger-router-controller: "controllers.processflow_management_controllers"
      summary: "get processFlow"
      parameters:
      - name: "processFlowId"
        required: true
        in: "path"
        type: "string"
      responses:
        "200":
          description: "Ok"
          schema:
            $ref: "#/definitions/ProcessFlow"
        "404":
          description: "Not Found"
          schema:
            $ref: "#/definitions/Error"


    patch:
      tags:
      - "ProcessFlow"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      operationId: "processflow_patch"
      x-swagger-router-controller: "controllers.processflow_management_controllers"
      summary: "patch processFlow"
      parameters:
      - name: "processFlowId"
        required: true
        in: "path"
        type: "string"
      responses:
        "200":
          description: "Ok"


    delete:
      tags:
      - "ProcessFlow"
      operationId: "processflow_delete"
      x-swagger-router-controller: "controllers.processflow_management_controllers"
      summary: "delete processFlow"
      parameters:
      - name: "processFlowId"
        required: true
        in: "path"
        type: "string"
      responses:
        "204":
          description: "No Content"

  /processFlow/{processFlowId}/taskFlow:
    get:
      tags:
      - "TaskFlow"
      produces:
      - "application/json"
      operationId: "get_taskflows_from_processflow"
      x-swagger-router-controller: "controllers.processflow_management_controllers"
      summary: "get taskFlows from processFlow Id"
      parameters:
      - name: "processFlowId"
        required: true
        in: "path"
        type: "string"
      responses:
        "200":
          description: "Ok"
          schema:
           type: "array"
           items:
             $ref: "#/definitions/TaskFlow"
        "404":
          description: "Not Found"
          schema:
            $ref: "#/definitions/Error"

  /taskFlow/{taskFlowId}:
    get:
      tags:
      - "TaskFlow"
      produces:
      - "application/json"
      operationId: "taskflow_get"
      x-swagger-router-controller: "controllers.processflow_management_controllers"
      summary: "get taskFlow"
      parameters:
      - name: "taskFlowId"
        required: true
        in: "path"
        type: "string"
      responses:
        "200":
          description: "Ok"
          schema:
            $ref: "#/definitions/TaskFlow"
        "404":
          description: "Not Found"
          schema:
            $ref: "#/definitions/Error"
       
    patch:
      tags:
      - "TaskFlow"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      operationId: "taskflow_patch"
      x-swagger-router-controller: "controllers.processflow_management_controllers"
      summary: "patch taskFlow"
      parameters:
      - name: "taskFlowId"
        required: true
        in: "path"
        type: "string"
      responses:
        "200":
          description: "Ok"




 
definitions:
  Any: {}
  ProcessFlow:
    required:
    - "id"
    - "href"
    - "state"
    - "processFlowSpecification"
    type: "object"
    properties:
      id:
        type: "string"
        description: "Identifier of the processFlow"
      href:
        type: "string"
        description: "Reference of the processFlow"
      processFlowDate:
        type: "string"
        format: "date-time"
        description: "Is the date when the processFlow was created in basse (timestamp)"
      processFlowSpecification:
        type: "string"
        description: "Specification of the processFlow."
      channel:
        type: "array"
        items:
          $ref: "#/definitions/ChannelRef"
        description: "A list of channel(s) where this processFlow is executed"
      characteristic:
        type: "array"
        items:
          $ref: "#/definitions/Characteristic"
        description: "A list of characteristic(s) associated to this processFlow"
      relatedEntity:
        type: "array"
        items:
          $ref: "#/definitions/RelatedEntity"
        description: "A list of related entity(ies) to this processFlow"
      relatedParty:
        type: "array"
        items:
          $ref: "#/definitions/RelatedParty"
        description: "A list of related party(ies) to this processFlow"
      state:
        $ref: "#/definitions/ProcessFlowStateType"
        description: "State of the processFlow: described in the state machine diagram."
      taskFlow:
        type: "array"
        items:
          $ref: "#/definitions/TaskFlowRef"
        description: "A list of taskflow related to this processFlow"
      '@baseType':
        type: "string"
        description: "When sub-classing, this defines the super-class"
      '@schemaLocation':
        type: "string"
        format: "uri"
        description: "A URI to a JSON-Schema file that defines additional attributes and relationships"
      '@type':
        type: "string"
        description: "When sub-classing, this defines the sub-class entity name"
  ChannelRef:
    type: "object"
    description: "The channel to which the resource reference to. e.g. channel for selling product offerings, channel for opening a trouble ticket etc.."
    properties:
      id:
        type: "string"
        description: "Unique identifier of a channel."
      href:
        type: "string"
        description: "Reference of the channel."
      name:
        type: "string"
        description: "Name of the channel."
      '@baseType':
        type: "string"
        description: "When sub-classing, this defines the super-class"
      '@schemaLocation':
        type: "string"
        format: "uri"
        description: "A URI to a JSON-Schema file that defines additional attributes and relationships"
      '@type':
        type: "string"
        description: "When sub-classing, this defines the sub-class entity name"
      '@referredType':
        type: "string"
        description: "The actual type of the target instance when needed for disambiguation."
    required:
      - "id"
  Characteristic:
    description: "Describes a given characteristic of an object or entity through a name/value pair."
    required:
    - "name"
    - "value"
    type: "object"
    properties:
      name:
        type: "string"
        description: "Name of the characteristic"
      valueType:
        type: "string"
        description: "Data type of the value of the characteristic"
      value:
        $ref: "#/definitions/Any"
        description: "The value of the characteristic"
      '@baseType':
        type: "string"
        description: "When sub-classing, this defines the super-class"
      '@schemaLocation':
        type: "string"
        format: "uri"
        description: "A URI to a JSON-Schema file that defines additional attributes and relationships"
      '@type':
        type: "string"
        description: "When sub-classing, this defines the sub-class entity name"
  RelatedEntity:
    type: "object"
    description: "A reference to an entity, where the type of the entity is not known in advance."
    required:
      - '@referredType'
      - "id"
      - "role"
    properties:
      id:
        type: "string"
        description: "Unique identifier of a related entity."
      href:
        type: "string"
        description: "Reference of the related entity."
      name:
        type: "string"
        description: "Name of the related entity."
      role:
        type: "string"
        description: "The role of an entity."
      '@baseType':
        type: "string"
        description: "When sub-classing, this defines the super-class"
      '@schemaLocation':
        type: "string"
        format: "uri"
        description: "A URI to a JSON-Schema file that defines additional attributes and relationships"
      '@type':
        type: "string"
        description: "When sub-classing, this defines the sub-class entity name"
      '@referredType':
        type: "string"
        description: "The actual type of the target instance when needed for disambiguation."
  RelatedParty:
    type: "object"
    description: "Related Entity reference. A related party defines party or party role linked to a specific entity."
    required:
      - "name"
    properties:
      id:
        type: "string"
        description: "Unique identifier of a related party."
      href:
        type: "string"
        description: "Reference of the related party."
      name:
        type: "string"
        description: "Name of the related party."
      role:
        type: "string"
        description: "Role played by the related party"
      '@baseType':
        type: "string"
        description: "When sub-classing, this defines the super-class"
      '@schemaLocation':
        type: "string"
        format: "uri"
        description: "A URI to a JSON-Schema file that defines additional attributes and relationships"
      '@type':
        type: "string"
        description: "When sub-classing, this defines the sub-class entity name"
      '@referredType':
        type: "string"
        description: "The actual type of the target instance when needed for disambiguation."
  ProcessFlowStateType:
    description: "Valid values for the lifecycle state of the processFlow"
    type: "string"
    enum:
    - "active"
    - "cancelled"
    - "hold"
    - "completed"
  TaskFlowRef:
    type: "object"
    properties:
      id:
        type: "string"
        description: "Unique identifier of a taskFlow."
      href:
        type: "string"
        description: "Reference of the taskFlow."
      '@baseType':
        type: "string"
        description: "When sub-classing, this defines the super-class"
      '@schemaLocation':
        type: "string"
        format: "uri"
        description: "A URI to a JSON-Schema file that defines additional attributes and relationships"
      '@type':
        type: "string"
        description: "When sub-classing, this defines the sub-class entity name"
      '@referredType':
        type: "string"
        description: "The actual type of the target instance when needed for disambiguation."
    required:
      - "id"

  TaskFlow:
    type: "object"
    required: 
      - "id"
      - "href"
      - "state"
      - "relatedParty"
    properties:
      name:
        type: "string"
        description: "Name of the taskFlow (specification)"
      id:
        type: "string"
        description: "Identifier of the taskFlow"
      href:
        type: "string"
        description: "Reference of the taskFlow"
      completionMethod:
        type: "string"
        description: "TaskFlow completion method."
      isMandatory:
        type: "boolean"
        description: "Indicate mandatory TaskFlow."
      priority:
        type: "integer"
        description: "TaskFlow priority."
      taskFlowSpecification:
        type: "string"
        description: "Specification of the taskFlow."
      channel:
        type: "array"
        items:
          $ref: "#/definitions/ChannelRef"
        description: "A list of channel(s) where this taskFlow is executed"
      characteristic:
        type: "array"
        items:
          $ref: "#/definitions/Characteristic"
        description: "A list of characteristic(s) associated to this taskFlow"
      relatedEntity:
        type: "array"
        items:
          $ref: "#/definitions/RelatedEntity"
        description: "A list of related entity(ies) to this taskFlow"
      relatedParty:
        type: "array"
        items:
          $ref: "#/definitions/RelatedParty"
        description: "A list of related party(ies) to this task"
      state:
        $ref: "#/definitions/TaskFlowStateType"
        description: "State of the taskFlow: described in the state machine diagram."
      taskFlowRelationship:
        type: "array"
        items:
          $ref: "#/definitions/TaskFlowRelationship"
        description: "A list of taskFlows related to this taskFlow"
      '@baseType':
        type: "string"
        description: "When sub-classing, this defines the super-class"
      '@schemaLocation':
        type: "string"
        format: "uri"
        description: "A URI to a JSON-Schema file that defines additional attributes and relationships"
      '@type':
        type: "string"
        description: "When sub-classing, this defines the sub-class entity name"
  TaskFlowStateType:
    description: "Valid values for the lifecycle state of the taskFlow"
    type: "string"
    enum:
    - "new"
    - "active"
    - "hold"
    - "cancelled"
    - "completed"  
  TaskFlowRelationship:
    type: "object"
    description: "Describes relationship between taskFlow"
    required:
      - "relationshipType"
      - "taskFlow"
    properties:
      relationshipType:
        type: "string"
        description: "The type of taskFlow relationship (requires, triggers, etc.)"
      taskFlow:
        $ref: "#/definitions/TaskFlowRef"
        description: "The taskFlow being referred to"
      '@baseType':
        type: "string"
        description: "When sub-classing, this defines the super-class"
      '@schemaLocation':
        type: "string"
        format: "uri"
        description: "A URI to a JSON-Schema file that defines additional attributes and relationships"
      '@type':
        type: "string"
        description: "When sub-classing, this defines the sub-class entity name"

  Error:
    description: "Used when the API throws an Error"
    type: "object"
    required:
      - "code"
      - "reason"
    properties:
      code:
        type: "integer"
        description: "An integer coding the error type"
      reason:
        type: "string"
        description: "Explanation of the reason for the error which can be shown to a client user."
      message:
        type: "string"
        description: "More details and corrective actions related to the error which can be shown to a client user."
      status:
        type: "string"
        description: "HTTP Error code extension"
      referenceError:
        type: "string"
        format: "uri"
        description: "URI of documentation describing the error."
      '@baseType':
        type: "string"
        description: "When sub-classing, this defines the super-class."
      '@schemaLocation':
        type: "string"
        format: "uri"
        description: "A URI to a JSON-Schema file that defines additional attributes and relationships"
      '@type':
        type: "string"
        description: "When sub-classing, this defines the sub-class entity name."

