# configure Service Resolver

each API module has its own configuration file

for the RFS-Spec API :

```bash
https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/api_rfs_spec_server/src/app_conf/config_default.yaml
```

for the CFS-Spec API:

```bash
https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/api_service_spec_server/src/app_conf/config_default.yaml
```

for service instance API:

```bash
https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/api_service_server/src/app_conf/config_default.yaml
```

for service order API:

```bash
https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/api_service_order_server/src/app_conf/config_default.yaml
```

for instantiation decision API:

```bash
https://gitlab.com/Orange-OpenSource/lfn/onap/service-resolver/blob/master/api_instantiation_decision_server/src/app_conf/config_default.yaml
```

Those files contains

proxy configuration to access ONAP API
simulation mode (true/false)
macro mode (true/false)
all URL/Headers infos to access ONAP API
all URL/port infos to access Mongodb API
all URL/port infos to access other Service Resolver API modules
debug level

To run Service Resolver, a docker-compose.yaml file is also used where
various hostname, port can be defined

That docker-compose.yaml file is common to all Service Resolver micro-services.

What to do if....

I want to use Service Resolver in "simulator" mode:

- nothing to change

I want to use Service Resolver connected to ONAP:

- if you have a proxy between Service Resolver and ONAP, you need to declare
  the proxy parameters in RFS-SPEC api, service instance api and
  ServiceOrder API modules (other modules do not communicate with ONAP)
- modify the "simulation mode" to "False" in RFS-SPEC api, service instance
  api and ServiceOrder API modules
- modify the "id_generated_by_server" parameter to "False" in service instance
  API module
  in the section service_resolver.service_api because those "id" values
  will be providedby ONAP
- modify the "macro" mode to "True" if you want to use that instantiation
  method from ONAP, otherwise it will be "A-la-Carte" method.
