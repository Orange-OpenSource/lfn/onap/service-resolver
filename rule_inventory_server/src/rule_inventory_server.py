#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""rule inventory API server
"""
from logging.config import dictConfig
import os
import connexion
from rule_inventory_logdict import conf_dict
from rule_inventory_utils import get_config

# Logger setup
CONF = conf_dict('rule_inventory.log')
dictConfig(CONF)

RULE_INVENTORY_API_SERVER = get_config("service_resolver.rule_inventory_api.server")
RULE_INVENTORY_API_HOST = os.environ['rule_inventory_server_hostname']
RULE_INVENTORY_API_PORT = os.environ['rule_inventory_server_port']
RULE_INVENTORY_API_DEBUG_MODE = get_config("service_resolver.rule_inventory_api.api_debug_mode")

API_RULE_INVENTORY_SERVER = connexion.App(__name__,
                                          specification_dir='swagger/')
API_RULE_INVENTORY_SERVER.add_api('swagger.yaml',
                                  strict_validation=True,
                                  validate_responses=True)
API_RULE_INVENTORY_SERVER.run(server=RULE_INVENTORY_API_SERVER,
                              host=RULE_INVENTORY_API_HOST,
                              port=RULE_INVENTORY_API_PORT,
                              debug=RULE_INVENTORY_API_DEBUG_MODE)
