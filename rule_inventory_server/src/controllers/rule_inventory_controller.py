#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""rule instance management
"""
import uuid
import logging
import json
import rule_inventory_exceptions as service_resolver_exceptions
from rule_inventory_utils import get_config
from rule_inventory_mongo_client import (mongo_find,
                                         mongo_find_one,
                                         mongo_find_one_and_update,
                                         mongo_delete)
LOGGER = logging.getLogger('rule_inventory_api')
RULE_ID = get_config("service_resolver.rule_inventory_api." +
                     "rule_id_generated_by_server")


def instantiationRuleCreate(InstantiationRule):   # pylint: disable=C0103
    """declare a rule
    """
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(InstantiationRule, indent=4, sort_keys=True))
    query = {'name': InstantiationRule["name"]}
    try:
        resp = mongo_find_one(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        # we generate a UUID
        if RULE_ID:
            InstantiationRule['instantiationRuleId'] = str(uuid.uuid4())
        # we insert the rule in the database
        query = {'instantiationRuleId':
                 InstantiationRule['instantiationRuleId']}
        try:
            resp = mongo_find_one_and_update(query, InstantiationRule, True)
        except service_resolver_exceptions.MongodbException:
            LOGGER.error("Unable to connect to Mongodb")
            return {"error": "Unable to connect to Mongodb"}, 503
        return resp, 201
    message_value = "rule : " + InstantiationRule["name"] + " already exists"
    message = {"message": message_value}
    LOGGER.debug('message : %s ', message_value)
    return message, 409


def instantiationRuleDelete(InstantiationRuleId):   # pylint: disable=C0103
    """delete rule
    """
    query = {'instantiationRuleId': InstantiationRuleId}
    try:
        resp = mongo_delete(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return {}, 204


def instantiationRuleFind():   # pylint: disable=C0103
    """find rule
    """
    query = {}
    try:
        resp = mongo_find(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    return resp, 200


def instantiationRuleGet(InstantiationRuleId):   # pylint: disable=C0103
    """get rule
    """
    query = {'instantiationRuleId': InstantiationRuleId}
    try:
        resp = mongo_find_one(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return resp, 200


def instantiationRuleUpdate(InstantiationRuleId,   # pylint: disable=C0103
                            InstantiationRule):
    """update rule
    """
    query = {'instantiationRuleId': InstantiationRuleId}
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(InstantiationRule, indent=4, sort_keys=True))
    try:
        resp = mongo_find_one_and_update(query, InstantiationRule, False)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return resp, 200
