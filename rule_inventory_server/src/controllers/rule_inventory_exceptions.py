#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

""" Module to define testing exceptions """

__author__ = ("Rene ROBERT <rene.robert@orange.com>")


class MongodbException(Exception):
    """Problem with mongodb communication"""
