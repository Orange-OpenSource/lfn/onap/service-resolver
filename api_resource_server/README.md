# resource inventory API server

## Overview

This API server is about managing resource instances in an inventory.

A resource can be of any type: physical, logical, virtual...

## Requirements

## configuration file

Configuration file is config_default.yaml

located in api_resource_server/src/app_conf

It contains mainly information about

* loglevel
* simulator mode
* proxy to get access to ONAP
* mongodb API information
* ONAP various API information
* other Service Resolver API information

## Running via python3

To run the server, please execute the following:

```bash
pip3 install -r requirements.txt
cd src
python3 api_resource_server.py
```

## Running with Docker

To run the server on a Docker container, please execute the following:

```bash
# building the image
docker build -t api_resource_server .

# starting up the container
# need /data repository on the hosts
docker run -p 6008:6008 --volume=/data:/data api_resource_server
```

## Use

open your browser to here:

```bash
http://localhost:6008/serviceResolver/resourceInventoryManagement/api/v1/ui/
```

Your Swagger definition lives here:

```bash
http://localhost:6008/serviceResolver/resourceInventoryManagement/api/v1/swagger.json
```
