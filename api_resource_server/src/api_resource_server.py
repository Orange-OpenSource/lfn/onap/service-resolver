#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
""" resource inventory api server
"""

from logging.config import dictConfig
import os
import connexion
from resource_logdict import conf_dict
from res_utils import get_config

# logging setup
CONF = conf_dict('resource_api.log')
dictConfig(CONF)

RESOURCE_API_SERVER = get_config("service_resolver.resource_api.server")
RESOURCE_API_HOST = get_config("service_resolver.resource_api.host")
RESOURCE_API_PORT = get_config("service_resolver.resource_api.port")
RESOURCE_API_HOST = os.environ['resource_server_hostname']
RESOURCE_API_PORT = os.environ['resource_server_port']
RESOURCE_API_DEBUG_MODE = get_config("service_resolver.resource_api.api_debug_mode")

API_RESOURCE_SERVER = connexion.App(__name__,
                                    specification_dir='swagger/')
API_RESOURCE_SERVER.add_api('swagger.yaml',
                            strict_validation=True,
                            validate_responses=True)

API_RESOURCE_SERVER.run(server=RESOURCE_API_SERVER,
                        host=RESOURCE_API_HOST,
                        port=RESOURCE_API_PORT,
                        debug=RESOURCE_API_DEBUG_MODE)
