#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""rfs specification related operations
"""
import os
import logging
import json
import uuid
from datetime import datetime
import pytz
from flask import render_template
from service_handling import service_instance_declare
from sorder_client_to_api import (delete_service,
                                  get_service,
                                  update_service,
                                  get_rfs_spec,
                                  send_order_to_nbi,
                                  send_order_to_so)
from sorder_mongo_client import mongo_find_one_and_update
from sorder_utils import get_config

LOGGER = logging.getLogger('service_order_api')
if LOGGER.hasHandlers():
    # Logger is already configured, remove all handlers
    LOGGER.handlers = []

MACRO = get_config("macro_mode")
SIMUL = get_config("simulator_mode")


def process_rfs_orders_one_by_one(order_item, solution, service_order):
    """process all rfs_order from solution
    """
    LOGGER.debug('call to process_rfs_orders_one_by_one function')
    for rfs_spec in solution["rfs_spec_list"]:
        # build rfsOrder that will be sent
        rfs_order = {}
        related_party = {}
        related_party["id"] = service_order["relatedParty"][0]["id"]
        related_party["name"] = service_order["relatedParty"][0]["name"]
        related_party["role"] = service_order["relatedParty"][0]["role"]
        order_item["service"]["relatedParty"] = []
        order_item["service"]["relatedParty"].append(related_party)
        # order_rfs
        response = post_rfs_order(rfs_spec,
                                  order_item,
                                  SIMUL,
                                  MACRO)
        # update rfs order with informations coming from response
        rfs_order["id"] = response["id"]
        rfs_order["href"] = response["href"]
        rfs_order["ordered_rfs_spec_id"] = rfs_spec["id"]
        rfs_order["ordered_rfs_spec_name"] = rfs_spec["name"]
        if "serviceSpecRelationship" in rfs_spec:
            tmp = rfs_spec["serviceSpecRelationship"]
            rfs_order["ordered_rfs_spec_relationships"] = tmp
        rfs_order["requested_action"] = response["orderItem"][0]["action"]
        rfs_order["order_item_state"] = response["orderItem"][0]["state"]
        rfs_order["@type"] = "extendedOrderItemRelationship"
        rfs_order["@baseType"] = "OrderItemRelationship"
        # here after is the rfs instance id
        serv_id = response["orderItem"][0]["service"]["id"]
        rfs_order["rfs_instance_id"] = serv_id
        # here after is the rfs instance name
        name = response["orderItem"][0]["service"]["name"]
        rfs_order["rfs_instance_name"] = name
        rfs_order["relationshipType"] = "cfsOrder_to_rfsOrder"
        # update cfs_order with rfs_order
        if "orderItemRelationship" not in order_item:
            order_item["orderItemRelationship"] = []
        order_item["orderItemRelationship"].append(rfs_order)
        relation = order_item["orderItemRelationship"]
        service_order["orderItem"][0]["orderItemRelationship"] = relation
    # here after we handle the state of each cfs, based of rfs state
    service_order["orderItem"][0]["state"] = "completed"
    # LOGGER.debug('\nservice_order :  %s\n',
    #              json.dumps(service_order, indent=4, sort_keys=True))
    if "orderItemRelationship" in service_order:
        for elem in service_order["orderItem"][0]["orderItemRelationship"]:
            if elem["order_item_state"] != "completed":
                tmp = elem["order_item_state"]
                service_order["orderItem"][0]["state"] = tmp
                break
    # here after we handle the service order state
    service_order["state"] = "completed"
    for elem in service_order["orderItem"]:
        if service_order["orderItem"][0]["state"] != "completed":
            service_order["state"] = service_order["orderItem"][0]["state"]
            break
    return service_order


def simul_post_order(order_item, rfs_spec):
    """in case of simul mode
    """
    LOGGER.debug('call to simul_post_order function')
    if order_item["action"] == "add":
        # here after we handle the "add" action
        test_data_path = os.path.dirname(os.path.abspath(__file__))
        test_data_path = test_data_path + "/data/"
        input_filename = "get_rfs_order_simu_response_add_completed.json"
        with open(test_data_path + input_filename) as file:
            mock = json.load(file)
        # in SIMUL mode we generate all IDs
        mock["id"] = str(uuid.uuid4())
        mock["href"] = ("serviceOrder/" + mock["id"])
        mock["orderItem"][0]["service"]["id"] = str(uuid.uuid4())
        name = rfs_spec["name"] + "_" + str(uuid.uuid4())
        mock["orderItem"][0]["service"]["name"] = name

        specid = mock["orderItem"][0]["service"]["serviceSpecification"]["id"]
        specid = rfs_spec["id"]
        mock["orderItem"][0]["service"]["serviceSpecification"]["id"] = specid

        name = mock["orderItem"][0]["service"]["serviceSpecification"]["name"]
        name = rfs_spec["name"]
        mock["orderItem"][0]["service"]["serviceSpecification"]["name"] = name

        related_party = {}
        related_party["id"] = order_item["service"]["relatedParty"][0]["id"]
        party_name = order_item["service"]["relatedParty"][0]["name"]
        related_party["name"] = party_name
        party_role = order_item["service"]["relatedParty"][0]["role"]
        related_party["role"] = party_role
        party = mock["orderItem"][0]["service"]["relatedParty"]
        party.append(related_party)
        mock["orderItem"][0]["service"]["relatedParty"] = party
        item = mock["orderItem"][0]
        LOGGER.debug('RFS MOCK %s :', mock)
        service = service_instance_declare(item,
                                           mock,
                                           "rfs")
        mock["orderItem"][0]["service"]["id"] = service["id"]
        LOGGER.debug('\nMOCK response RFS ORDER :\n%s\n',
                     json.dumps(mock, indent=4, sort_keys=True))
        return mock
    # here after we handle the "delete" action
    test_data_path = os.path.dirname(os.path.abspath(__file__))
    test_data_path = test_data_path + "/data/"
    input_filename = "get_rfs_order_simu_response_delete_completed.json"
    with open(test_data_path + input_filename) as file:
        mock = json.load(file)
    mock["id"] = str(uuid.uuid4())
    mock["href"] = ("serviceOrder/" + mock["id"])
    mock["orderItem"][0]["service"]["id"] = order_item["service"]["id"]
    mock["orderItem"][0]["service"]["name"] = order_item["service"]["name"]
    delete_service(mock["orderItem"][0]["id"])
    return mock


def item_object(item, onap_platform):
    """update item
    """
    # item["service"]["@type"] = "Service"
    # item["service"]["@schemaLocation"] = ""
    # item["service"]["@baseType"] = "Service"
    # item["service"]["href"] = ""
    item["service"]["supportingResource"] = []
    # here after we add the onap platform that support that instance
    tmp = onap_platform
    # item["service"]["supportingResource"][0] = tmp
    item["service"]["supportingResource"].append(tmp)
    # item["service"]["serviceCharacteristic"] = []
    # item["service"]["serviceSpecification"]["@type"] = ""
    # item["service"]["serviceSpecification"]["@schemaLocation"] = ""
    # item["service"]["serviceSpecification"]["@baseType"] = ""
    # item["service"]["serviceSpecification"]["href"] = ""
    # item["service"]["serviceSpecification"]["name"] = ""
    # item["service"]["serviceSpecification"]["targetServiceSchema"] = ""
    # item["service"]["serviceSpecification"]["version"] = ""
    return item


def generate_rfs_order_resp(so_order_resp, party, payload):
    """build an rfs_order response from an ONAP SO response
    """
    utc = pytz.utc
    now_utc = datetime.now(utc).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
    rfs_order_resp = {}
    rfs_order_resp["id"] = so_order_resp["requestReferences"]["requestId"]
    rfs_order_resp["href"] = "soOrder/" + rfs_order_resp["id"]
    rfs_order_resp["externalId"] = ""
    rfs_order_resp["priority"] = ""
    rfs_order_resp["description"] = ""
    rfs_order_resp["category"] = "rfs"
    rfs_order_resp["state"] = ""
    rfs_order_resp["orderDate"] = now_utc
    rfs_order_resp["completionDateTime"] = ""
    rfs_order_resp["requestedStartDate"] = ""
    rfs_order_resp["requestedCompletionDate"] = ""
    rfs_order_resp["startDate"] = ""
    rfs_order_resp["@baseType"] = "Service"
    rfs_order_resp["@type"] = "Service"
    rfs_order_resp["@schemaLocation"] = ""
    rfs_order_resp["relatedParty"] = []
    related_party = generate_related_party(party)
    rfs_order_resp["relatedParty"].append(related_party)
    rfs_order_resp["orderRelationship"] = ""
    rfs_order_resp["orderItem"] = []
    order_item = generate_order_item(so_order_resp, payload, related_party)
    rfs_order_resp["orderItem"].append(order_item)
    rfs_order_resp["orderMessage"] = ""
    return rfs_order_resp


def generate_related_party(party):
    """build related_party data structure
    """
    related_party = {}
    related_party["id"] = party[0]["id"]
    related_party["href"] = ""
    related_party["role"] = party[0]["role"]
    related_party["name"] = party[0]["name"]
    related_party["@referredType"] = ""
    return related_party


def generate_order_item(so_order_resp, payload, related_party):
    """build order_item data structure
    """
    order_item = {}
    order_item["orderMessage"] = ""
    order_item["id"] = "1"
    order_item["action"] = "add"
    order_item["state"] = "acknowledged"
    order_item["percentProgress"] = "0"
    order_item["@type"] = ""
    order_item["@schemaLocation"] = ""
    order_item["@baseType"] = ""
    order_item["orderItemRelationship"] = []
    order_item["service"] = {}
    tmp = so_order_resp["requestReferences"]["instanceId"]
    order_item["service"]["id"] = tmp
    order_item["service"]["href"] = ""
    tmp = payload["requestDetails"]["requestInfo"]["instanceName"]
    order_item["service"]["name"] = tmp
    order_item["service"]["serviceState"] = ""
    order_item["service"]["@type"] = ""
    order_item["service"]["@schemaLocation"] = ""
    order_item["service"]["serviceCharacteristic"] = []
    order_item["service"]["serviceRelationship"] = ""
    order_item["service"]["relatedParty"] = []
    order_item["service"]["relatedParty"].append(related_party)
    order_item["service"]["serviceSpecification"] = {}
    tmp = payload["requestDetails"]["modelInfo"]["modelVersionId"]
    order_item["service"]["serviceSpecification"]["id"] = tmp
    order_item["service"]["serviceSpecification"]["href"] = ""
    tmp = payload["requestDetails"]["modelInfo"]["modelName"]
    order_item["service"]["serviceSpecification"]["name"] = tmp
    tmp = payload["requestDetails"]["modelInfo"]["modelVersion"]
    order_item["service"]["serviceSpecification"]["version"] = tmp
    order_item["service"]["serviceSpecification"]["targetServiceSchema"] = ""
    order_item["service"]["serviceSpecification"]["@type"] = ""
    order_item["service"]["serviceSpecification"]["@schemaLocation"] = ""
    order_item["service"]["serviceSpecification"]["@baseType"] = ""
    return order_item


def real_post_order_add(order_item, rfs_spec, payload, macro):
    """in case of add action
    """
    LOGGER.debug('processing add action')
    # here after we handle the "add" action
    onap_platform_name = rfs_spec["supportingResource"][0]["name"]
    if not macro:
        # here after we generate an instance name
        name = (payload["orderItem"][0]["service"]["name"] + "_" +
                str(uuid.uuid4()))
        payload["orderItem"][0]["service"]["name"] = name
        response = send_order_to_nbi(payload, onap_platform_name)
        rfs_order_resp = response.copy()
    else:
        response = send_order_to_so("", "add", payload, onap_platform_name)
        so_order_resp = response.copy()
    # here after we copy the rfs instance in Service Resolver
    # note that ONAP provides all the IDs
    related_party = {}
    related_party["id"] = order_item["service"]["relatedParty"][0]["id"]
    party_name = order_item["service"]["relatedParty"][0]["name"]
    related_party["name"] = party_name
    party_role = order_item["service"]["relatedParty"][0]["role"]
    related_party["role"] = party_role
    party = []
    party.append(related_party)
    if not macro:
        rfs_order_resp["orderItem"][0]["service"]["relatedParty"] = party
    else:
        rfs_order_resp = generate_rfs_order_resp(so_order_resp,
                                                 party,
                                                 payload)
    item = item_object(rfs_order_resp["orderItem"][0],
                       rfs_spec["supportingResource"][0])
    service_instance_declare(item,
                             rfs_order_resp,
                             "rfs")
    return rfs_order_resp


def real_post_order(order_item, rfs_spec, payload, macro):
    """in case of not in simul mode
    """
    LOGGER.debug('call to real_post_order function')
    if order_item["action"] == "add":
        rfs_order_resp = real_post_order_add(order_item,
                                             rfs_spec,
                                             payload,
                                             macro)

    # here after we handle the "delete" action
    # onap platform name
    onap_platform_name = rfs_spec["supportingResource"][0]["name"]
    if not macro:
        payload["id"] = str(uuid.uuid4())
        payload["href"] = ("serviceOrder/" + payload["id"])
        # related party information
        related_party = {}
        related_party["id"] = order_item["service"]["relatedParty"][0]["id"]
        party_name = order_item["service"]["relatedParty"][0]["name"]
        related_party["name"] = party_name
        party_role = order_item["service"]["relatedParty"][0]["role"]
        related_party["role"] = party_role
        party = []
        party.append(related_party)
        payload["relatedParty"] = party
        # order item
        payload["orderItem"][0]["service"]["id"] = order_item["service"]["id"]
        tmp = order_item["service"]["name"]
        payload["orderItem"][0]["service"]["name"] = tmp
        # service specification
        tmp = rfs_spec["id"]
        payload["orderItem"][0]["service"]["serviceSpecification"]["id"] = tmp
        response = send_order_to_nbi(payload, onap_platform_name)
        delete_service(payload["orderItem"][0]["id"])
        return response
    # here after we handle delete in macro mode
    LOGGER.debug('processing delete action')
    so_order_resp = send_order_to_so(order_item["service"]["id"],
                                     "delete",
                                     payload, onap_platform_name)
    # related party information
    related_party = {}
    related_party["id"] = order_item["service"]["relatedParty"][0]["id"]
    party_name = order_item["service"]["relatedParty"][0]["name"]
    related_party["name"] = party_name
    party_role = order_item["service"]["relatedParty"][0]["role"]
    related_party["role"] = party_role
    party = []
    party.append(related_party)
    payload["relatedParty"] = party
    rfs_order_resp = generate_rfs_order_resp(so_order_resp,
                                             party,
                                             payload)
    delete_service(order_item["service"]["id"])
    return rfs_order_resp


def post_rfs_order(rfs_spec,
                   order_item,
                   simul,
                   macro):
    """send service Order to ONAP NBI
    """
    LOGGER.debug('call to post_rfs_order function')
    if order_item["action"] == "add" and not macro:
        payload_template = 'rfs_order_payload_alacarte.json'
    elif order_item["action"] == "delete" and not macro:
        payload_template = 'rfs_order_payload_delete.json'
    elif macro:
        payload_template = 'rfs_order_payload_macro.json'
    rfs_spec = get_rfs_spec(rfs_spec["id"])
    naming_id = "SR_" + str(uuid.uuid4())
    body_string = render_template(
        payload_template,
        rfs_spec=rfs_spec,
        order_item=order_item,
        naming_id=naming_id)
    payload = json.loads(body_string)
    LOGGER.debug('\nMACRO payload that will be sent to ONAP SO :\n%s\n',
                 json.dumps(payload, indent=4, sort_keys=True))
    # here after we have the switch between simulator mode or real mode
    if not simul:
        rfs_order_resp = real_post_order(order_item, rfs_spec, payload, macro)
        return rfs_order_resp
    mock = simul_post_order(order_item, rfs_spec)
    return mock


def rfs_full_delete(cfs,
                    rfs,
                    rfs_spec,
                    rfs_order_item,
                    service_order):
    """full rfs delete via rfsOrder
    """
    LOGGER.debug('call to rfs_full_delete function')
    # there is no other cfs related to the rfs, we send delete order
    response = post_rfs_order(rfs_spec,
                              rfs_order_item,
                              SIMUL,
                              MACRO)
    LOGGER.debug('\nresponse after post_rfs_order:\n%s\n',
                 json.dumps(response, indent=4, sort_keys=True))
    # update rfs order with informations coming from response
    rfs_order = {}
    rfs_order["id"] = response["id"]
    rfs_order["href"] = response["href"]
    rfs_order["action"] = response["orderItem"][0]["action"]
    rfs_order["state"] = response["state"]
    rfs_order["relationshipType"] = "cfsOrder_to_rfsOrder"
    # update cfs_order with the new rfs_order
    if "orderItemRelationship" not in rfs_order_item:
        rfs_order_item["orderItemRelationship"] = []
    rfs_order_item["orderItemRelationship"].append(rfs_order)
    relation = rfs_order_item["orderItemRelationship"]
    service_order["orderItem"][0]["orderItemRelationship"] = relation
    # DEPRECATED service_order["orderRelationship"].append(rfs_order)
    # store cfs_order in database
    query = {'id': service_order['id']}
    mongo_find_one_and_update(query, service_order, True)
    # remove rfs relationship in cfs
    cfs["supportingService"].remove(rfs)
    # update cfs in service_api
    get_service(cfs["id"])
    update_service(cfs["id"], cfs)

    # delete rfs in service_API
    delete_service(rfs["id"])
    return cfs


def rfs_delete_orders(order_item, service_order):
    """process all rfs_order delete except if the rfs instance is in use
       with multiple cfs (we check the number of cfs relationships)
    """
    LOGGER.debug('call to rfs_delete_orders function')
    cfs_id = order_item["service"]["id"]
    # request to service API
    response = get_service(cfs_id)
    cfs = response
    rfs_list = cfs["supportingService"].copy()
    for rfs in rfs_list:
        # request to service API
        response = get_service(rfs["id"])
        stored_rfs = response
        # rfs_spec = stored_rfs["serviceSpecification"]
        rfs_order_item = order_item.copy()
        rfs_order_item["service"] = stored_rfs
        if len(stored_rfs["serviceRelationship"]) == 1:
            # there is no other cfs related to the rfs, we send delete order
            cfs = rfs_full_delete(cfs,
                                  rfs,
                                  stored_rfs["serviceSpecification"],
                                  rfs_order_item,
                                  service_order)
        else:
            # the rfs in use by multiple cfs_spec
            # we only delete the relationships : no delete rfs operation
            #
            # find an remove the relationship in rfs
            for elem in stored_rfs["serviceRelationship"]:
                if elem["id"] == cfs["id"]:
                    stored_rfs["serviceRelationship"].remove(elem)
                    break
            # request to update in service API
            response = update_service(stored_rfs["id"], stored_rfs)
            # remove rfs relationship in cfs
            cfs["supportingService"].remove(rfs)
    # request to update in service API
    response = update_service(cfs["id"], cfs)
    return service_order
