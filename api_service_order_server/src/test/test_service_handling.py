#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""test service (cfs) instance related operations
"""
from service_handling import service_instance_declare


# def test_service_instance_declare(requests_mock):
#     """test
#     """
#     order_item = {}
#     order_item["service"] = {}
#     service_order = {}
#     service_order["id"] = "00"
#     service_order["orderDate"] = "000"
#     order_item["action"] = "add"
#     expected_response = ({}, 201)
#     host_port = "http://localhost:6003"
#     url = host_port + "/service"
#     requests_mock.post(url, status_code=201, json={})
#     response = service_instance_declare(order_item, service_order)
#     assert response == expected_response
