#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""test for orderItems related operations
"""
import json
# from orderitems_handling import all_order_item_are_valid, build_cfs_spec_list


# def test_all_order_item_are_valid_ok(requests_mock):
#     """test
#     """
#     cfs_spec_list = [{"id": "140646745461600",
#                       "name": "vBox_FixedAccess",
#                       "version": "1"}]
#     expected_bad_list_result = []
#     host_port = "http://localhost:6004"
#     url = host_port + "/serviceSpecification/140646745461600"
#     requests_mock.get(url, status_code=200)
#     result = all_order_item_are_valid(cfs_spec_list)
#     assert result == expected_bad_list_result


# def test_all_order_item_are_valid_nok(requests_mock):
#     """test
#     """
#     cfs_spec_list = [{"id": "140646745461600",
#                       "name": "vBox_FixedAccess",
#                       "version": "1"}]
#     expected_bad_list_result = ["vBox_FixedAccess"]
#     host_port = "http://localhost:6004"
#     url = host_port + "/serviceSpecification/140646745461600"
#     requests_mock.get(url, status_code=404)
#     result = all_order_item_are_valid(cfs_spec_list)
#     assert result == expected_bad_list_result


# def test_build_cfs_spec_list_with_empty_list():
#     """test
#     """
#     service_order = {}
#     expected_result = []
#     result = build_cfs_spec_list(service_order)
#     assert result == expected_result


# def test_build_cfs_spec_list_with_vbox_example():
#     """test
#     """
#     test_data_path = "src/test/data/"
#     input_filename = "service_order_vbox.json"
#     result_filename = "cfs_spec_list.json"
#     with open(test_data_path + input_filename) as file:
#         service_order = json.load(file)
#     with open(test_data_path + result_filename) as file:
#         expected_result = json.load(file)
#     result = build_cfs_spec_list(service_order)
#     assert result == expected_result
