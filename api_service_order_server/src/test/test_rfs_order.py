#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""tests rfs_order
"""
import json
import pytest
# from rfs_order import post_rfs_order, get_rfs_order, process_rfs_orders


# def test_post_rfs_order_ok(requests_mock, mocker):
#     """test
#     """
#     rfs_spec = {}
#     rfs_spec["id"] = "0000000"
#     rfs_spec["name"] = "superlan_rfs_spec"

#     order_item = {}
#     order_item["action"] = "add"
#     order_item["service"] = {}
#     order_item["service"]["id"] = "0001"
#     order_item["service"]["name"] = "cfs_test_0001"
#     order_item["service"]["relatedParty"] = "Festina"

#     mocked_response = {}
#     mocked_response["id"] = "0001"

#     mocked_rfs_order = {}
#     mocked_rfs_order["orderItem"] = []
#     mocked_rfs_order_item = {}
#     mocked_rfs_order_item["state"] = "completed"
#     mocked_rfs_order["orderItem"].append(mocked_rfs_order_item)

#     expected_response = {"orderItem": [{"state": "completed"}]}

#     expected_status_code = 201
#     host_port = "http://nbi.api.simpledemo.onap.org:30274"
#     url = host_port + "/nbi/api/v3/serviceOrder"

#     func_where_is_func_to_mock = 'rfs_order'
#     func_to_mock = 'render_template'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value={})

#     requests_mock.post(url, status_code=201, json=mocked_response)

#     func_where_is_func_to_mock = 'rfs_order'
#     func_to_mock = 'get_rfs_order'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=mocked_rfs_order)

#     response, status_code = post_rfs_order(rfs_spec, order_item)
#     assert status_code == expected_status_code
#     assert response == expected_response


# def test_get_rfs_order(requests_mock):
#     """test
#     """
#     rfs_order_id = "000"
#     expected_response = {}
#     base = "http://nbi.api.simpledemo.onap.org:30274/nbi/api/v3/serviceOrder"
#     url = base + "/" + rfs_order_id
#     requests_mock.get(url, status_code=201, json={})
#     response = get_rfs_order(rfs_order_id)
#     assert response == expected_response


# @pytest.mark.parametrize("post_rfs_order_mock_status_code, expected_message, expected_service_order_file", [
#     (201, "all rfs order taken into account by ONAP", "service_order_vbox_with_rfs_orders.json" ),
#     (404, "failed to order rfs : NERG-Tunnel-Conf : error", "service_order_vbox_failed_with_rfs_orders.json")
# ])
# def test_process_rfs_orders(mocker,
#                             post_rfs_order_mock_status_code,
#                             expected_message,
#                             expected_service_order_file):
#     """test
#     """
#     test_data_path = "src/test/data/"

#     input_filename = "order_item_vbox.json"
#     with open(test_data_path + input_filename) as file:
#         order_item = json.load(file)

#     input_filename = "solution_vbox.json"
#     with open(test_data_path + input_filename) as file:
#         solution = json.load(file)

#     input_filename = "service_order_vbox.json"
#     with open(test_data_path + input_filename) as file:
#         service_order = json.load(file)

#     input_filename = "rfs_order_completed.json"
#     with open(test_data_path + input_filename) as file:
#         post_rfs_order_mock_response = json.load(file)

#     input_filename = expected_service_order_file
#     with open(test_data_path + input_filename) as file:
#         expected_service_order = json.load(file)

#     func_where_is_func_to_mock = 'rfs_order'
#     func_to_mock = 'post_rfs_order'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=[post_rfs_order_mock_response,
#                                post_rfs_order_mock_status_code])

#     message, service_order = process_rfs_orders(order_item,
#                                                 solution,
#                                                 service_order)
#     print("service_order  =  ", json.dumps(service_order, indent=1))
#     assert message == expected_message
#     assert service_order == expected_service_order
