#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""tests for functions that handle rfs from cfs_order
"""
import json
import re
import pytest
# from decision_operations import (compare,
#                                  select_decision,
#                                  rfs_instance_in_a_set,
#                                  rfs_instance_exists_in_a_set,
#                                  param_in_cfs_order,
#                                  rfs_instance_capacity,
#                                  perform_decision)


# @pytest.mark.parametrize("val1, operator, val2, expected_result", [
#     (1, "equal", 1, True),
#     (2, "greater", 1, True),
#     (1, "less", 2, True),
#     (True, "not_equal", False, True),
#     ("1", "fake", "1", False),
# ])
# def test_compare(val1, operator, val2, expected_result):
#     """test
#     """
#     result = compare(val1, operator, val2)
#     assert result == expected_result


# def test_select_decision_ok():
#     """test
#     """
#     rule_results = [{"rule_response": True,
#                      "decision": "do_not_instantiate"},
#                     {"rule_response": False,
#                      "decision": "instantiate"}]
#     rule_response = True
#     expected_decision = "do_not_instantiate"
#     decision = select_decision(rule_results, rule_response)
#     assert decision == expected_decision


# def test_select_decision_empty():
#     """test
#     """
#     rule_results = []
#     rule_response = True
#     expected_decision = "no_decision"
#     decision = select_decision(rule_results, rule_response)
#     assert decision == expected_decision


# def test_param_in_cfs_order_ok():
#     """ test
#     """
#     order_item = {}
#     test_data_path = "src/test/data/"
#     filename = "service_order_vbox.json"
#     with open(test_data_path + filename) as file:
#         order = json.load(file)
#     order_item = order["orderItem"][0]


#     filename = "cfs_spec_vbox.json"
#     with open(test_data_path + filename) as file:
#         cfs_spec_vbox = json.load(file)

#     for rfs_spec in cfs_spec_vbox["serviceSpecRelationship"]:
#         if rfs_spec["name"] == "SuperLAN_virtualNetwork":
#             for rfs_instantiationDecisionRule in rfs_spec["rfs_instantiationDecisionRules"]:
#                 if rfs_instantiationDecisionRule["rule_operation"] == "param_in_cfs_order":
#                     rfs_spec_rule_params = rfs_instantiationDecisionRule["rule_params"]
#                     rfs_spec_rule_results = rfs_instantiationDecisionRule["rule_results"]
#     decision, rfs = param_in_cfs_order(order_item,
#                                        rfs_spec_rule_params,
#                                        rfs_spec_rule_results)
#     assert decision == "instantiate"
#     assert rfs is None


# @pytest.mark.parametrize("customer_id, rfs_set_id_key", [
#     ("", "all"),
#     ("Festina", "customer_id")
# ])
# def test_rfs_instance_in_a_set(customer_id, rfs_set_id_key, requests_mock):
#     """tests
#     """
#     order_item = {}
#     order_item["service"] = {}
#     order_item["service"]["relatedParty"] = {}
#     order_item["service"]["relatedParty"]["name"] = customer_id
#     rule_params = {}
#     rule_params["rfs_set_id_key"] = rfs_set_id_key
#     rule_params["rfs_spec_name"] = "vBoxVNF"
#     test_data_path = "src/test/data/"
#     response_filename = "rfs_instances_all_no_vBoxVNF.json"
#     with open(test_data_path + response_filename) as file:
#         rfs_instance_list_example = json.load(file)
#     matcher = re.compile('/service')
#     requests_mock.get(matcher, json=rfs_instance_list_example)
#     response = rfs_instance_in_a_set(order_item, rule_params)
#     expected_response = rfs_instance_list_example
#     assert response == expected_response


# @pytest.mark.parametrize("response_filename, customer_value, rfs_spec_name, rfs_set_id_key, rule_response_0, decision_0, rule_response_1, decision_1, expected_decision", [
#     ("rfs_instances_Festina_nothing.json", "Festina", "SuperLAN_virtualNetwork", "customer_id", True, "do_not_instantiate", False, "instantiate", "instantiate"),
#     ("rfs_instances_Festina_no_SuperLAN.json", "Festina", "SuperLAN_virtualNetwork", "customer_id", True, "do_not_instantiate", False, "instantiate", "instantiate"),
#     ("rfs_instances_Festina_SuperLAN.json", "Festina", "SuperLAN_virtualNetwork", "customer_id", True, "do_not_instantiate", False, "instantiate", "do_not_instantiate"),
#     ("rfs_instances_all_vBoxVNF.json", "fake", "vBoxVNF", "all", True, "do_not_instantiate", False, "instantiate", "do_not_instantiate"),
#     ("rfs_instances_all_no_vBoxVNF.json", "fake", "vBoxVNF", "all", True, "do_not_instantiate", False, "instantiate", "instantiate")
# ])
# def test_rfs_instance_exists_in_a_set(response_filename,
#                                       customer_value,
#                                       rfs_spec_name,
#                                       rfs_set_id_key,
#                                       rule_response_0,
#                                       decision_0,
#                                       rule_response_1,
#                                       decision_1,
#                                       expected_decision,
#                                       mocker):
#     """tests various situations
#     """
#     order_item = {}
#     order_item["service"] = {}
#     order_item["service"]["relatedParty"] = {}
#     order_item["service"]["relatedParty"]["name"] = customer_value
#     rule_params = {}
#     rule_params["rfs_spec_name"] = rfs_spec_name
#     rule_params["rfs_set_id_key"] = rfs_set_id_key
#     rule_results = []
#     rule_result = {}
#     rule_result["rule_response"] = rule_response_0
#     rule_result["decision"] = decision_0
#     rule_results.append(rule_result)
#     rule_result = {}
#     rule_result["rule_response"] = rule_response_1
#     rule_result["decision"] = decision_1
#     rule_results.append(rule_result)

#     test_data_path = "src/test/data/"
#     with open(test_data_path + response_filename) as file:
#         rfs_instance_response_example = json.load(file)

#     func_where_is_func_to_mock = 'decision_operations'
#     func_to_mock = 'rfs_instance_in_a_set'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=rfs_instance_response_example)
#     decision, rfs_list = rfs_instance_exists_in_a_set(order_item,
#                                                       rule_params,
#                                                       rule_results)
#     assert decision == expected_decision


# @pytest.mark.parametrize("capacity, filename, expected_decision", [
#     (1, "rfs_instances_all_vBoxVNF.json", "instantiate"),
#     (400, "rfs_instances_all_vBoxVNF.json", "do_not_instantiate"),
#     (400, "rfs_instances_all_no_vBoxVNF.json", "instantiate")])
# def test_rfs_instance_capacity(capacity, filename, expected_decision, mocker):
#     """test
#     """
#     test_data_path = "src/test/data/"
#     input_filename = "service_order_vbox.json"
#     with open(test_data_path + input_filename) as file:
#         service_order = json.load(file)
#     order_item = service_order["orderItem"][0]
#     rule_params = {"rfs_spec_name": "vBoxVNF",
#                    "rfs_set_key": "all"}
#     rule_params["capacity"] = capacity
#     rule_results = [{"rule_response": True,
#                      "decision": "do_not_instantiate"},
#                     {"rule_response": False,
#                      "decision": "instantiate"}]
#     test_data_path = "src/test/data/"

#     with open(test_data_path + filename) as file:
#         rfs_instance_response_example = json.load(file)

#     func_where_is_func_to_mock = 'decision_operations'
#     func_to_mock = 'rfs_instance_in_a_set'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=rfs_instance_response_example)
#     decision, response = rfs_instance_capacity(order_item,
#                                                rule_params,
#                                                rule_results)
#     for rfs_instance in rfs_instance_response_example:
#         if rfs_instance["name"] == "vBoxVNF-service-instance-9J26TO":
#             expected_response = rfs_instance
#         else:
#             expected_response = response
#     assert (decision == expected_decision) and (response == expected_response)


# @pytest.mark.parametrize("rule_operation, expected_decision", [
#     ("rfs_instance_capacity", ("instantiate", {})),
#     ("rfs_instance_exists_in_a_set", ("instantiate", {})),
#     ("param_in_cfs_order", ("instantiate", None)),
#     ("undefined_rule_operation", ("no_decision", None))])
# def test_perform_decision_ok(rule_operation, expected_decision, mocker):
#     """execute the rule_operation
#     """
#     order_item = {}
#     rule_params = {}
#     rule_results = {}
#     func_where_is_func_to_mock = 'decision_operations'
#     func_to_mock = rule_operation
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=expected_decision)
#     decision = perform_decision(order_item,
#                                 rule_params,
#                                 rule_results,
#                                 rule_operation)
#     assert decision == expected_decision
