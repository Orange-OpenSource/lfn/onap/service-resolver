#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""  tests for rfs spec client to api operation
"""
import pytest
import requests
from sorder_utils import get_config

ONAP_NBI_API_BASE_URL = get_config("onap.nbi.url")
ONAP_NBI_API_HEADER = get_config("onap.nbi.headers")
ONAP_SDC_API_BASE_URL = get_config("onap.sdc.url")
ONAP_SDC_API_DOWNLOAD_HEADER = get_config("onap.sdc.download_headers")

