#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""tests build solution
"""
import json
from build_solution import build_solution


# def test_build_solution_with_vbox(requests_mock, mocker):
#     """test
#     """
#     test_data_path = "src/test/data/"
#     input_filename = "service_order_vbox.json"
#     with open(test_data_path + input_filename) as file:
#         service_order = json.load(file)
#     order_item = service_order["orderItem"][0]

#     host_port = "http://localhost:6004"
#     url = host_port + "/serviceSpecification/140646745461600"

#     input_filename = "cfs_spec_vbox.json"
#     with open(test_data_path + input_filename) as file:
#         cfs_spec_vbox = json.load(file)

#     result_filename = "solution_vbox_all_rfs.json"
#     with open(test_data_path + result_filename) as file:
#         expected_result = json.load(file)

#     requests_mock.get(url, json=cfs_spec_vbox)
#     func_where_is_func_to_mock = 'build_solution'
#     func_to_mock = 'perform_decision'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=['instantiate', {}])
#     result = build_solution(order_item)
#     print(json.dumps(result, indent=1))
#     assert result == expected_result
