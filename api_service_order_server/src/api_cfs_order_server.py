#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""bss-gateway CFS_ORDER api
"""
from logging.config import dictConfig
import os
import connexion
from sorder_logdict import conf_dict
from sorder_utils import get_config

# Logger setup
CONF = conf_dict('service_order.log')
dictConfig(CONF)

CFS_ORDER_API_SERVER = get_config("service_resolver.service_order_api.server")
CFS_ORDER_API_HOST = os.environ['service_order_server_hostname']
CFS_ORDER_API_PORT = os.environ['service_order_server_port']
CFS_ORDER_API_DEBUG_MODE = get_config("service_resolver.service_order_api.api_debug_mode")

API_CFS_ORDER_SERVER = connexion.App(__name__,
                                     specification_dir='swagger/')
API_CFS_ORDER_SERVER.add_api('swagger.yaml',
                             strict_validation=True,
                             validate_responses=True)
API_CFS_ORDER_SERVER.run(server=CFS_ORDER_API_SERVER,
                         host=CFS_ORDER_API_HOST,
                         port=CFS_ORDER_API_PORT,
                         debug=False)
