# serviceOrder API server

## Overview

This API server is about managing the various service Order
to be usable by a BSS.

Those service orders are composed of orderItems that point to service models
from the service catalog.

The service catalog contains customer facing service specifications.

This API is about ordering cfs based on cfs specification.

For each requested cfs, the API server will get from the service catalog
the rfs spec list that is composing the cfs spec.

A service order composent of the several rfs spec to be delivered
will be then sent to ONAP NBI component.

The API server will take care if there is a need to produce the various rfs
in a logical order based on a "priority" attribute.

The API server will also take into account is there are some instantiation
conditions defined in the service definition.

## Requirements

## configuration file

Configuration file is config_default.yaml

located in api_service_order_server/src/app_conf

It contains mainly information about

* loglevel
* simulator mode
* proxy to get access to ONAP
* mongodb API information
* ONAP various API information
* other Service Resolver API information

## Running via python3

To run the server, please execute the following:

```bash
pip3 install -r requirements.txt
cd src
python3 api_service_order_server.py
```

## Running with Docker

To run the server on a Docker container, please execute the following:

```bash
# building the image
docker build -t api_service_order_server .

# starting up the container
# need /data repository on the hosts
docker run -p 6002:6002 --volume=/data:/data api_service_order_server
```

## Use

open your browser to here:

```bash
http://localhost:6002/serviceResolver/serviceOrderManagement/api/v1/ui/
```

Your Swagger definition lives here:

```bash
http://localhost:6002/serviceResolver/serviceOrderManagement/api/v1/swagger.json
```
