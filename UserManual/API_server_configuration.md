API server configuration
========================

Each API server that is part of Service Resolver has its own configuration file.

this file is located in:

service-resolver/api_XXX_server/src/app_conf

File name is **config_default.yaml**.

It contains a lot of parameters that can be adapted to the deployment context.

Part of those parameters:

- LogLevel
- simulator_mode
- timezone
- proxies
- hostname/port/db
- onap url(s)
- service_resolver api parameters
