# service (rfs and cfs) inventory API server

## Overview

This API server is about managing service instances in an inventory.

Those service are  called customer facing
service  (so called "cfs" and "rfs" from TM Forum wording)

It is not possible to create/delete directly a service.

It is necessary to use the service Order API to request the creation/deletion
of a service.

For maintenance purpose only, create/delete are implemented.

## Requirements

## configuration file

Configuration file is config_default.yaml

located in api_service_server/src/app_conf

It contains mainly information about

* loglevel
* simulator mode
* proxy to get access to ONAP
* mongodb API information
* ONAP various API information
* other Service Resolver API information

## Running via python3

To run the server, please execute the following:

```bash
pip3 install -r requirements.txt
cd src
python3 api_service_server.py
```

## Running with Docker

To run the server on a Docker container, please execute the following:

```bash
# building the image
docker build -t api_service_server .

# starting up the container
# need /data repository on the hosts
docker run -p 6003:6003 --volume=/data:/data api_service_server
```

## Use

open your browser to here:

```bash
http://localhost:6003/serviceResolver/serviceInventoryManagement/api/v1/ui/
```

Your Swagger definition lives here:

```bash
http://localhost:6003/serviceResolver/serviceInventoryManagement/api/v1/swagger.json
```
