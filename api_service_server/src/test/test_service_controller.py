#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""
    test file for service_controler
"""
import json
import pytest
from service_controller import (serviceCreate,
                                serviceDelete,
                                serviceFind,
                                serviceGet,
                                serviceUpdate) # pylint: disable=C0103


# @pytest.mark.parametrize("resp, expected_status", [
#     ({}, 409),
#     (None, 201)
# ])
# def test_service_create(mocker,
#                         resp,
#                         expected_status):
#     """test
#     """
#     service = {}
#     service["name"] = "test"
#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=resp)

#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one_and_update'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=service)
#     response, status_code = serviceCreate(service)
#     assert status_code == expected_status


# def test_service_delete(mocker):
#     """test
#     """
#     ServiceId = "0000"

#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'delete_one'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=[{}, 204])
#     response = serviceDelete(ServiceId)
#     assert response[0] == {}
#     assert response[1] == 204


# def test_service_find(mocker):
#     """test
#     """

#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=[])

#     response = serviceFind()
#     assert response[0] == []
#     assert response[1] == 200


# @pytest.mark.parametrize("resp, expected_resp, expected_status", [
#     ({"key": "value"}, True, 200),
#     (None, None, 404)
# ])
# def test_service_get(mocker, resp, expected_resp, expected_status):
#     """test
#     """
#     ServiceId = "0000"
#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=resp)

#     func_where_is_func_to_mock = 'service_controller'
#     func_to_mock = 'update_service_infos'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=True)

#     response = serviceGet(ServiceId)
#     assert response[0] == expected_resp
#     assert response[1] == expected_status


# def test_service_update_ok(mocker):
#     """test
#     """
#     ServiceId = "0001"
#     Service = {
#         "description": "vBox Service for Orange Spain",
#         "name": "vBox_FixedAccess_0001",
#         "version": "1.0",
#         "id": "0001"}
#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one_and_update'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=Service)
#     response = serviceUpdate(ServiceId,
#                              Service)
#     assert response[0] == Service
#     assert response[1] == 200


# def test_service_update_nok(mocker):
#     """test
#     """
#     ServiceId = "0000"
#     Service = {
#         "description": "vBox Service for Orange Spain",
#         "name": "vBox_FixedAccess_0001",
#         "version": "1.0",
#         "id": "0001"}
#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one_and_update'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=None)
#     response = serviceUpdate(ServiceId,
#                              Service)
#     assert response[0] == None
#     assert response[1] == 404
