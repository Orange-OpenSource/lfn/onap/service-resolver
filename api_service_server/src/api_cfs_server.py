#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
""" rfs and cfs inventory api server
"""

from logging.config import dictConfig
import os
import connexion
from service_logdict import conf_dict
from service_utils import get_config

# logging setup
CONF = conf_dict('service_api.log')
dictConfig(CONF)

SERVICE_API_SERVER = get_config("service_resolver.service_api.server")
SERVICE_API_HOST = get_config("service_resolver.service_api.host")
SERVICE_API_PORT = get_config("service_resolver.service_api.port")
SERVICE_API_HOST = os.environ['service_server_hostname']
SERVICE_API_PORT = os.environ['service_server_port']
SERVICE_API_DEBUG_MODE = get_config("service_resolver.service_api.api_debug_mode")

API_SERVICE_SERVER = connexion.App(__name__,
                                   specification_dir='swagger/')
API_SERVICE_SERVER.add_api('swagger.yaml',
                           strict_validation=True,
                           validate_responses=True)

API_SERVICE_SERVER.run(server=SERVICE_API_SERVER,
                       host=SERVICE_API_HOST,
                       port=SERVICE_API_PORT,
                       debug=SERVICE_API_DEBUG_MODE)
