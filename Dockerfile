FROM postman/newman

ENV HTTP_PROXY http://127.0.0.1:3128/
ENV HTTPS_PROXY http://127.0.0.1:3128/
#ENV http_proxy http://127.0.0.1:3128/

RUN apk update
RUN apk add bash
RUN apk add coreutils
RUN apk add jq
RUN apk add tzdata
#RUN npm install --silent -g newman-reporter-htmlextra
RUN npm install -g newman-reporter-htmlextra

