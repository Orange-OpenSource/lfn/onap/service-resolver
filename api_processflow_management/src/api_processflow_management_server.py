#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

""" PROCESSFLOW Management api
"""

from logging.config import dictConfig
import os
import connexion
#from processflow_management_logdict import conf_dict
from processflow_management_utils import get_config

# logging setup
#CONF = conf_dict('processflow_management.log')
#dictConfig(CONF)

PROCESSFLOW_API_SERVER = get_config("service_resolver.processflow_management_api.server")
PROCESSFLOW_API_HOST = os.environ['processflow_management_server_hostname']
PROCESSFLOW_API_PORT = os.environ['processflow_management_server_port']


API_PROCESSFLOW_MANAGEMENT_SERVER = connexion.App(__name__,
                                       specification_dir='swagger/')
API_PROCESSFLOW_MANAGEMENT_SERVER.add_api('swagger.yaml',
                               strict_validation=True,
                               validate_responses=True)
API_PROCESSFLOW_MANAGEMENT_SERVER.run(server=PROCESSFLOW_API_SERVER,
                           host=PROCESSFLOW_API_HOST,
                           port=PROCESSFLOW_API_PORT)
