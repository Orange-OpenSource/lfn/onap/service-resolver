#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

""" Camunda ProcessFlows and TaskFlows retrieving functions
"""
import json
import requests


def processflow_convert_tmf(processflow, camunda_id, url_tasks, url_date):
    """ Conversion of raw Camunda ProcessFlow Data into TMF friendly data
    """
    if processflow is None:
        resp = None
    else:
        new_processflow = {"href": 0, "id": 0, "state": 0,
                           "processFlowSpecification": 0,
                           "processFlowDate": 0,
                           "taskFlow": []}
        new_processflow["href"] = ("/ProcessFlow/" + processflow["id"])
        new_processflow["id"] = processflow["id"]
        new_processflow["processFlowSpecification"] = processflow["definitionId"].split(":")[0]
        payload = {}
        headers = {}
        camunda_tasks = requests.request("GET",
                                         url_tasks,
                                         headers=headers,
                                         data=json.dumps(payload))
        if camunda_tasks is not None:
            camunda_tasks = camunda_tasks.json()
        for i in camunda_tasks:
            if i["processInstanceId"] == camunda_id:
                task = {"id": i["id"],
                        "href": new_processflow["href"] +
                        "/TaskFlow/" + i["id"],
                        "@type": "TaskFlow"}
                new_processflow["taskFlow"].append(task)

        payload = {}
        headers = {}
        camunda_history = requests.request("GET",
                                           url_date,
                                           headers=headers,
                                           data=json.dumps(payload))
        if camunda_history is not None:
            camunda_history = camunda_history.json()
            for i in camunda_history:
                if i["id"] == camunda_id:
                    date = i["startTime"]
        new_processflow["processFlowDate"] = date

        if processflow["ended"] is False and processflow["suspended"] is False:
            new_processflow["state"] = "active"
        resp = new_processflow

    return resp


def taskflow_convert_tmf(taskflow):
    """ Conversion of raw Camunda TaskFlow Data into TMF friendly data
    """
    if taskflow is None:
        resp = None
    else:
        new_taskflow = {"href": 0,
                        "id": 0,
                        "state": 0,
                        "priority": 0,
                        "taskFlowSpecification": 0,
                        "relatedParty": []}
        new_taskflow["href"] = ("/ProcessFlow/" +
                                taskflow["processInstanceId"] +
                                "/TaskFlow/" +
                                taskflow["id"])
        new_taskflow["id"] = taskflow["id"]
        new_taskflow["priority"] = taskflow["priority"]
        new_taskflow["taskFlowSpecification"] = taskflow["taskDefinitionKey"]
        relatedPartyCamunda = {"name": taskflow["assignee"]}
        new_taskflow["relatedParty"].append(relatedPartyCamunda)
        new_taskflow["name"] = taskflow["name"]

        if taskflow["suspended"] is False:
            new_taskflow["state"] = "active"
        if taskflow["suspended"] is True:
            new_taskflow["state"] = "hold"
        resp = new_taskflow
    return resp


def upload_from_camunda(url, camunda_id):
    """ Request to Camunda API Rest
    """
    payload = {}
    headers = {}
    camunda_data = requests.request("GET",
                                    url,
                                    headers=headers,
                                    data=json.dumps(payload))
    resp = None
    if camunda_data is not None:
        camunda_data = camunda_data.json()
        for i in camunda_data:
            if i["id"] == camunda_id:
                resp = i
    return resp


def get_process_instance(url_get, url_tasks, url_date, camunda_id):
    """ Function that upload ProcessFlow data from Camunda
    API and then convert it into TMF Data
    """

    camunda_data = upload_from_camunda(url_get, camunda_id)
    tmf_data = processflow_convert_tmf(camunda_data,
                                       camunda_id,
                                       url_tasks,
                                       url_date)
    return tmf_data


def get_task_instance(url, camunda_id):
    """ Function that upload TaskFlow data from Camunda
    API and then convert it into TMF Data
    """

    camunda_data = upload_from_camunda(url, camunda_id)
    tmf_data = taskflow_convert_tmf(camunda_data)
    return tmf_data


def get_task_instance_from_processFlowId(url):
    """ Function that upload TaskFlow data from Camunda
    API and then convert it into TMF Data
    """
    resp = []
    payload = {}
    headers = {}
    camunda_data = requests.request("GET",
                                    url,
                                    headers=headers,
                                    data=json.dumps(payload))
    if camunda_data is not None:
        camunda_data = camunda_data.json()

    for i in camunda_data:
        tmf_data = taskflow_convert_tmf(i)
        resp.append(tmf_data)

    return resp
