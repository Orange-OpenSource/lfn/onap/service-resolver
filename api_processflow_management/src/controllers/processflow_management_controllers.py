#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

"""ProcessFlow Management related operations
"""

import json
import requests
from requests_camunda import (get_process_instance,
                              get_task_instance,
                              get_task_instance_from_processFlowId)
from processflow_management_client_to_api import (URL_REQUEST_CREATE_PROCESS,
                                                  URL_REQUEST_GET_PROCESS,
                                                  URL_REQUEST_DELETE_PROCESS,
                                                  URL_REQUEST_GET_TASK,
                                                  URL_REQUEST_GET_TASK_PROCESSFLOW,
                                                  URL_REQUEST_PATCH_TASK,
                                                  URL_REQUEST_HISTORY)


def processflow_create(processFlowSpecification):
    """ to do a POST request of a ProcessFlow in Camunda
    """

    url = (URL_REQUEST_CREATE_PROCESS +
           "/" + processFlowSpecification +
           "/start")

    """ example of data for the ProcessFlow "Payment-Retrieval"
        (to do : improve with "characteristic" value)
    """

    data = {"variables":
            {"amount":
             {"value": 555, "type": "long"},
             "item":
             {"value": "item-xyz"}
             }
            }
    headers = {"Content-Type": "application/json"}
    response = requests.request("POST",
                                url,
                                headers=headers,
                                data=json.dumps(data))

    if response is not None:
        return ({"code": 201, "reason": "Created"}), 201

    else:
        return ({"code": 400, "reason": "Could not create the ProcessFlow instance"}), 400


def processflow_get(processFlowId):
    """ to do a GET request of a ProcessFlow in Camunda
    """

    resp = get_process_instance(URL_REQUEST_GET_PROCESS,
                                URL_REQUEST_GET_TASK,
                                URL_REQUEST_HISTORY,
                                processFlowId)
    if resp is None:
        return ({"code": 404, "reason": "Not found"}), 404
    return resp, 200


def processflow_delete(processFlowId):
    """ to do a DELETE request of a ProcessFlow in db
    """

    url = (URL_REQUEST_DELETE_PROCESS)

    data = {"deleteReason": "no reason provided",
            "processInstanceIds": [processFlowId],
            "skipCustomListeners": True,
            "skipSubprocesses": True
            }

    headers = {"Content-Type": "application/json"}
    response = requests.request("POST",
                                url,
                                headers=headers,
                                data=json.dumps(data))
    return ({"code": 204, "reason": "Deleted"}), 204


def taskflow_get(taskFlowId):
    """ to do a GET request of a TaskFlow in Camunda
    """
    resp = get_task_instance(URL_REQUEST_GET_TASK,
                             taskFlowId)
    if resp is None:
        return ({"code": 404, "reason": "Not found"}), 404
    return resp, 200


def get_taskflows_from_processflow(processFlowId):
    """ to do a GET request of TaskFlows linked to a ProcessFlow instance in Camunda
    """
    url = URL_REQUEST_GET_TASK_PROCESSFLOW + processFlowId
    resp = get_task_instance_from_processFlowId(url)

    if resp is None:
        return ({"code": 404, "reason": "Not found"}), 404
    return resp, 200


def taskflow_patch(taskFlowId):
    """ to do a PATCH request of a TaskFlow in Camunda
    """
    url = (URL_REQUEST_PATCH_TASK + "/" + taskFlowId)

    """ example of data
    """
    data = {
            "name": "Payment Approval",
            "description": "This have to be done very urgent",
            "priority": 30,
            "assignee": "peter",
            "owner": "mary",
            "delegationState": "PENDING",
            "due": "2021-08-30T10:00:00.000+0200",
            "followUp": "2021-08-25T10:00:00.000+0200"
            }
    headers = {"Content-Type": "application/json"}
    response = requests.request("PUT",
                                url,
                                headers=headers,
                                data=json.dumps(data))
