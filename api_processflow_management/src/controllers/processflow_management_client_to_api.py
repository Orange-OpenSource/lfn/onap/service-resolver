#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

"""Camunda requests parameters recuperation from config_default.yaml
"""

from processflow_management_utils import get_config


URL_REQUEST_GET_PROCESS = ("http://" +
                           str(get_config("camunda.host")) +
                           ":" + str(get_config("camunda.port")) +
                           get_config("camunda.url") +
                           get_config("camunda.get_process_instance"))

URL_REQUEST_CREATE_PROCESS = ("http://" +
                              str(get_config("camunda.host")) +
                              ":" + str(get_config("camunda.port")) +
                              get_config("camunda.url") +
                              get_config("camunda.create_process_instance"))

URL_REQUEST_DELETE_PROCESS = ("http://" +
                              str(get_config("camunda.host")) +
                              ":" + str(get_config("camunda.port")) +
                              get_config("camunda.url") +
                              get_config("camunda.delete_process_instance"))

URL_REQUEST_GET_TASK = ("http://" +
                        str(get_config("camunda.host")) +
                        ":" + str(get_config("camunda.port")) +
                        get_config("camunda.url") +
                        get_config("camunda.get_task_instance"))

URL_REQUEST_GET_TASK_PROCESSFLOW = ("http://" +
                                    str(get_config("camunda.host")) +
                                    ":" +
                                    str(get_config("camunda.port")) +
                                    get_config("camunda.url") +
                                    get_config("camunda.taskfromprocessflowid"))

URL_REQUEST_PATCH_TASK = ("http://" +
                          str(get_config("camunda.host")) +
                          ":" + str(get_config("camunda.port")) +
                          get_config("camunda.url") +
                          get_config("camunda.patch_task_instance"))

URL_REQUEST_HISTORY = ("http://" +
                       str(get_config("camunda.host")) +
                       ":" + str(get_config("camunda.port")) +
                       get_config("camunda.url") +
                       get_config("camunda.history_process_instance"))
