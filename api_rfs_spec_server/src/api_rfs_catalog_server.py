#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

""" RFS CATALOG api
"""

from logging.config import dictConfig
import os
import connexion
from rfs_spec_logdict import conf_dict
from rfs_spec_utils import get_config

# logging setup
CONF = conf_dict('rfs_spec.log')
dictConfig(CONF)

RFS_SPEC_API_SERVER = get_config("service_resolver.rfs_spec_api.server")
RFS_SPEC_API_HOST = os.environ['rfs_catalog_server_hostname']
RFS_SPEC_API_PORT = os.environ['rfs_catalog_server_port']
RFS_SPEC_API_DEBUG_MODE = get_config("service_resolver.rfs_spec_api.api_debug_mode")

API_RFS_CATALOG_SERVER = connexion.App(__name__,
                                       specification_dir='swagger/')
API_RFS_CATALOG_SERVER.add_api('swagger.yaml',
                               strict_validation=True,
                               validate_responses=True)
API_RFS_CATALOG_SERVER.run(server=RFS_SPEC_API_SERVER,
                           host=RFS_SPEC_API_HOST,
                           port=RFS_SPEC_API_PORT,
                           debug=RFS_SPEC_API_DEBUG_MODE)
