#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""
    test file for rfs_specification_controler
"""
import pytest
import rfs_spec_exceptions as service_resolver_exceptions
from rfs_specification_controller import (update_rfs_spec_with_onap_data,
                                          update_data_in_db,
                                          rfsSpecificationCreate,
                                          rfsSpecificationDelete,
                                          rfsSpecificationFind,
                                          rfsSpecificationGet,
                                          rfsSpecificationUpdate,
                                          statusFind)

def test_update_rfs_spec_with_onap_data(mocker):
    """test update rfs spec  with Onap data
    """
    rfs_spec = {}
    rfs_spec["id"] = "0001"
    rfs_spec["supportingResource"] = []
    onap = {"name":"onap_1"}
    rfs_spec["supportingResource"].append(onap)
    fake_response = {}
    fake_response["id"] = "0001"
    fake_result = {}
    fake_result["path"] = ""
    fake_result["csar_filename"] = ""
    fake_filename = ""
    fake_rfs_spec_from_tosca = {}

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'get_rfs_spec_from_onap'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=fake_response)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'download_tosca_service_template'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=fake_result)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'extract_tosca_service_template'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=fake_filename)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'tosca_to_rfs_spec_func'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=fake_rfs_spec_from_tosca)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one_and_update'
    mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one_and_update'
    mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)

    update_rfs_spec_with_onap_data(rfs_spec)

def test_update_data_in_db(mocker):
    """test for update RfsSpecification in db
    """
    rfs_specification = {}
    rfs_specification["id"] = "0001"
    expected_response = {}
    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one_and_update'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value={})
    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value={})
    response = update_data_in_db(rfs_specification)
    assert response == expected_response


def test_rfsSpecificationCreate(mocker):   # pylint: disable=C0103
    """test for the insertion in the db
    """
    rfs_specification = {}
    rfs_specification["id"] = "0001"
    rfs_specification["name"] = "fake_name"
    expected_response = {}
    expected_status_code = 201

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'get_config'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=True)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=None)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'update_rfs_spec_with_onap_data'
    mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'update_data_in_db'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value={})

    response, code = rfsSpecificationCreate(rfs_specification)
    assert response == expected_response
    assert code == expected_status_code


def test_rfsSpecificationCreate_not_None(mocker):   # pylint: disable=C0103
    """test for the insertion in the db
    """
    rfs_specification = {}
    rfs_specification["id"] = "0001"
    rfs_specification["name"] = "fake_name"
    expected_status_code = 409
    expected_message = {"message": "rfs_spec : fake_name already exists"}

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value={})

    message, code = rfsSpecificationCreate(rfs_specification)
    assert code == expected_status_code
    assert message == expected_message


def test_rfsSpecificationCreate_mongo_exception_1(mocker):   # pylint: disable=C0103
    """test create function
    """
    rfs_specification = {}
    rfs_specification["name"] = "fake_name"
    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = service_resolver_exceptions.MongodbException
    result, status = rfsSpecificationCreate(rfs_specification)
    assert result == {"error": "Unable to connect to Mongodb"}
    assert status == 503

def test_rfsSpecificationCreate_mongo_exception_2(mocker):   # pylint: disable=C0103
    """test for the insertion in the db
    """
    rfs_specification = {}
    rfs_specification["id"] = "0001"
    rfs_specification["name"] = "fake_name"

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'get_config'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=True)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=None)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'update_rfs_spec_with_onap_data'
    mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'update_data_in_db'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = service_resolver_exceptions.MongodbException
    result, status = rfsSpecificationCreate(rfs_specification)
    assert result == {"error": "Unable to connect to Mongodb"}
    assert status == 503


def test_rfsSpecificationCreate_nbi_exception(mocker):   # pylint: disable=C0103
    """test create function
    """
    rfs_specification = {}
    rfs_specification['id'] = "0001"
    rfs_specification["name"] = "fake_name"
    rfs_specification['distributionStatus'] = ""
    rfs_specification["@type"] = "RfsSpecification"
    rfs_specification["@baseType"] = "ServiceSpecification"
    rfs_specification["@schemaLocation"] = ""

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'get_config'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=False)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=None)

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'update_rfs_spec_with_onap_data'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = service_resolver_exceptions.NbiException("toto")
    message, status = rfsSpecificationCreate(rfs_specification)
    assert message == {"problem": 'toto'}
    assert status == 503


def test_rfsSpecificationFind(mocker):   # pylint: disable=C0103
    """test of the find fonction of the controller
    """
    expected_response = {}
    expected_status_code = 200
    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value={})

    response, status_code = rfsSpecificationFind()
    assert response == expected_response
    assert status_code == expected_status_code


def test_rfsSpecificationFind_mongo_exception(mocker):   # pylint: disable=C0103
    """test of the find fonction of the controller
    """
    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = service_resolver_exceptions.MongodbException
    message, status = rfsSpecificationFind()
    assert message == {"error": "Unable to connect to Mongodb"}
    assert status == 503


def test_rfsSpecificationGet(mocker):  # pylint: disable=C0103
    """test of the get fonction of the controller
    """
    rfs_specification_id = ""
    expected_response = {}
    expected_status_code = 200

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value={})

    response, status_code = rfsSpecificationGet(rfs_specification_id)
    assert response == expected_response
    assert status_code == expected_status_code


def test_rfsSpecificationGet_resp_none(mocker):  # pylint: disable=C0103
    """test of the get fonction of the controller
    """
    rfs_specification_id = ""
    expected_response = {}
    expected_status_code = 404

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=None)

    response, status_code = rfsSpecificationGet(rfs_specification_id)
    assert response == expected_response
    assert status_code == expected_status_code


def test_rfsSpecificationGet_mongo_exception(mocker):   # pylint: disable=C0103
    """test get function
    """
    rfs_specification_id = ""
    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = service_resolver_exceptions.MongodbException
    message, status = rfsSpecificationGet(rfs_specification_id)
    assert message == {"error": "Unable to connect to Mongodb"}
    assert status == 503


def test_rfsSpecificationDelete(mocker):  # pylint: disable=C0103
    """test of the delete fonction of the controller
    """
    rfs_specification_id = ""
    expected_response = {}
    expected_status_code = 204

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_delete'
    mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)

    response, stat_code = rfsSpecificationDelete(rfs_specification_id)
    assert stat_code == expected_status_code
    assert response == expected_response


def test_rfsSpecificationDelete_mongo_exception(mocker):   # pylint: disable=C0103
    """test get function
    """
    rfs_specification_id = ""
    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_delete'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = service_resolver_exceptions.MongodbException
    message, status = rfsSpecificationDelete(rfs_specification_id)
    assert message == {"error": "Unable to connect to Mongodb"}
    assert status == 503


def test_rfsSpecificationDelete_None(mocker):  # pylint: disable=C0103
    """test of the delete fonction of the controller
    """
    rfs_specification_id = ""
    expected_response = None
    expected_status_code = 404

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_delete'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=None)

    response, stat_code = rfsSpecificationDelete(rfs_specification_id)
    assert stat_code == expected_status_code
    assert response == expected_response


def test_rfsSpecificationUpdate(mocker):  # pylint: disable=C0103
    """test of the update fonction of the controller
    """
    rfs_spec_id = ""

    expected_response = {}
    expected_status_code = 200

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one_and_update'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock), return_value={})
    response, status_code = rfsSpecificationUpdate(rfs_spec_id, {})
    assert status_code == expected_status_code
    assert response == expected_response


def test_rfsSpecificationUpdate_None(mocker):  # pylint: disable=C0103
    """test of the update fonction of the controller
    """
    rfs_spec_id = ""

    expected_response = {}
    expected_status_code = 404

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one_and_update'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock), return_value=None)

    response, status_code = rfsSpecificationUpdate(rfs_spec_id, {})
    assert status_code == expected_status_code
    assert response == expected_response


def test_rfsSpecificationUpdate_mongo_exception(mocker):   # pylint: disable=C0103
    """test get function
    """
    rfs_specification_id = ""
    rfs_specification = {}
    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find_one_and_update'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = service_resolver_exceptions.MongodbException
    message, status = rfsSpecificationUpdate(rfs_specification_id, rfs_specification)
    assert message == {"error": "Unable to connect to Mongodb"}
    assert status == 503

def test_statusFind_simul_mode(mocker):  # pylint: disable=C0103
    """test of the statusFind fonction of the controller
    """
    expected_response = {}
    expected_response["name"] = "rfs_specification_api"
    expected_response["status"] = "ok"
    expected_response["version"] = "v1"
    expected_response["communications"] = {}
    expected_response["communications"]["ONAP_platforms"] = []
    expected_response["communications"]["get_ONAP_nbi_status"] = "simulator mode"
    expected_response["communications"]["get_mongodb_status"] = "ok"
    expected_status_code = 200
    onap_platforms = [
    {
      "name": "onap_1",
      "proxies": {}
    },
    {
      "name": "onap_2",
      "proxies": {}
    }
    ]
    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'get_config'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = [True, onap_platforms]

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock))

    response, status_code = statusFind()
    assert status_code == expected_status_code
    assert response == expected_response


def test_statusFind_real_mode(mocker):  # pylint: disable=C0103
    """test of the statusFind fonction of the controller
    """
    expected_response = {}
    expected_response["name"] = "rfs_specification_api"
    expected_response["status"] = "ok"
    expected_response["version"] = "v1"
    expected_response["communications"] = {}
    expected_response["communications"]["ONAP_platforms"] = [{'get_ONAP_nbi_status': 'ok', 'name': 'onap_1', 'proxies': {}}, {'get_ONAP_nbi_status': 'ok', 'name': 'onap_2', 'proxies': {}}]
    expected_response["communications"]["get_mongodb_status"] = "ok"
    expected_status_code = 200
    onap_platforms = [
    {
      "name": "onap_1",
      "proxies": {}
    },
    {
      "name": "onap_2",
      "proxies": {}
    }
    ]
    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'get_config'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = [False, onap_platforms]

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'nbi_healthcheck'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock))

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock))

    response, status_code = statusFind()
    assert status_code == expected_status_code
    assert response == expected_response


def test_statusFind_nbi_exception(mocker):   # pylint: disable=C0103
    """test statusFind function
    """
    onap_platforms = [{
      "name": "onap_1",
      "proxies": {}
    }]
    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'get_config'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = [False, onap_platforms]

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'nbi_healthcheck'
    my_mock = mocker.patch((func_where_is_func_to_mock + "." + func_to_mock))
    my_mock.side_effect = service_resolver_exceptions.NbiException    

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock))

    response, status = statusFind()
    assert response["status"] == "NOK"
    assert status == 200


def test_statusFind_mongo_exception(mocker):   # pylint: disable=C0103
    """test statusFind function
    """
    onap_platforms = [
    {
      "name": "onap_1",
      "proxies": {}
    },
    {
      "name": "onap_2",
      "proxies": {}
    }
    ]
  
    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'get_config'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = [True, onap_platforms]

    func_where_is_func_to_mock = 'rfs_specification_controller'
    func_to_mock = 'mongo_find'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = service_resolver_exceptions.MongodbException
    response, status = statusFind()
    assert response["status"] == "problem with MongoDB"
    assert status == 200
