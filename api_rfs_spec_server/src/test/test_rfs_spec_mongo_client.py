#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""
    test file for mongo_client
"""
import pytest
import rfs_spec_exceptions as service_resolver_exceptions
from rfs_spec_mongo_client import (mongo_find_one,
                                   mongo_find_one_and_update,
                                   mongo_delete,
                                   mongo_find)


def test_mongo_find_one(mocker):
    """test mongo_find_one
    """
    query = {'id': "001"}
    fake_response = {'id': '001', 'name': 'toto1'}
    func_where_is_func_to_mock = 'rfs_spec_mongo_client'
    func_to_mock = 'RFS_SPEC.find_one'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=fake_response)
    respon = mongo_find_one(query)
    assert respon == fake_response


def test_mongo_find_one_exception(mocker):
    """test mongo_find_one exception
    """
    query = {'id': "001"}
    message = "problem with MongoDB communication"
    func_where_is_func_to_mock = 'rfs_spec_mongo_client'
    func_to_mock = 'RFS_SPEC.find_one'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = Exception
    with pytest.raises(Exception, match=message):
        mongo_find_one(query)


def test_mongo_find_one_and_update(mocker):
    """test mongo_find_one_and_update
    """
    query = {'id': "001"}
    new_thing = {'id': '001', 'name': 'toto1_new'}
    upsert = "upsert"
    fake_response = {'id': '001', 'name': 'toto1'}

    func_where_is_func_to_mock = 'rfs_spec_mongo_client'
    func_to_mock = 'RFS_SPEC.create_index'
    mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)

    func_where_is_func_to_mock = 'rfs_spec_mongo_client'
    func_to_mock = 'RFS_SPEC.find_one_and_update'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=fake_response)

    response = mongo_find_one_and_update(query,
                                         new_thing,
                                         upsert)
    assert response == fake_response


def test_mongo_find_one_and_update_exception(mocker):
    """test mongo_find_one_and_update exception
    """
    query = {'id': "001"}
    new_thing = {'id': '001', 'name': 'toto1_new'}
    upsert = "upsert"
    fake_response = {'id': '001', 'name': 'toto1'}

    message = "problem with MongoDB communication"

    func_where_is_func_to_mock = 'rfs_spec_mongo_client'
    func_to_mock = 'RFS_SPEC.create_index'
    mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)

    func_where_is_func_to_mock = 'rfs_spec_mongo_client'
    func_to_mock = 'RFS_SPEC.find_one_and_update'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = Exception
    with pytest.raises(Exception, match=message):
        mongo_find_one_and_update(query, new_thing, upsert)


def test_mongo_delete(mocker):
    """test mongo_delete
    """
    query = None
    expected_response = {}
    func_where_is_func_to_mock = 'rfs_spec_mongo_client'
    func_to_mock = 'RFS_SPEC.delete_one'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=None)
    response = mongo_delete(query)
    assert response == expected_response


def test_mongo_delete_exception(mocker):
    """test mongo_delete exception
    """
    query = None
    message = "problem with MongoDB communication"
    func_where_is_func_to_mock = 'rfs_spec_mongo_client'
    func_to_mock = 'RFS_SPEC.delete_one'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = Exception
    with pytest.raises(Exception, match=message):
        mongo_delete(query)


def test_mongo_find(mocker):
    """test mongo_find
    """
    query = {'id': "001"}
    fake_response = [{'id': '001', 'name': 'toto1'}]
    expected_response = [{'id': '001', 'name': 'toto1'}]
    func_where_is_func_to_mock = 'rfs_spec_mongo_client'
    func_to_mock = 'RFS_SPEC.find'
    mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
                 return_value=fake_response)
    response = mongo_find(query)
    assert response == expected_response


def test_mongo_find_exception(mocker):
    """test mongo_find exception
    """
    query = {'id': "001"}
    message = "problem with MongoDB communication"
    func_where_is_func_to_mock = 'rfs_spec_mongo_client'
    func_to_mock = 'RFS_SPEC.find'
    my_mock = mocker.patch(func_where_is_func_to_mock + "." + func_to_mock)
    my_mock.side_effect = Exception
    with pytest.raises(Exception, match=message):
        mongo_find(query)
