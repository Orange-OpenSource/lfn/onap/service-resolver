# service spec (CFS spec) catalog API server

## Overview

This API server is about managing the various service models
to be exposed to BSS.

Those service models are then called customer facing
service specification (so called "cfs specification" from TM Forum wording)

This API is then a cfs catalog.

A cfsSpec is composed of rfsSpec. This API allow to declare such kind of
relations.

rfsSpec are defined in ONAP SDC component.

cfs Specification is not accepted if rfs specification are not present in
ONAP SDC.

## Requirements

## configuration file

Configuration file is config_default.yaml

located in api_service_spec_server/src/app_conf

It contains mainly information about

* loglevel
* simulator mode
* proxy to get access to ONAP
* mongodb API information
* ONAP various API information
* other Service Resolver API information

## Running via python3

To run the server, please execute the following:

```bash
pip3 install -r requirements.txt
cd src
python3 api_service_spec_server.py
```

## Running with Docker

To run the server on a Docker container, please execute the following:

```bash
# building the image
docker build -t api_service_spec_server .

# starting up the container
# need /data repository on the hosts
docker run -p 6004:6004 --volume=/data:/data api_service_spec_server
```

## Use

open your browser to here:

```bash
http://localhost:6004/serviceResolver/serviceCatalogManagement/api/v1/ui/
```

Your Swagger definition lives here:

```bash
http://localhost:6004/serviceResolver/serviceCatalogManagement/api/v1/swagger.json
```
