#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""tests cfs_spec_handling
"""
import pytest
from service_specification_controller import (serviceSpecificationCreate,
                                              serviceSpecificationDelete,
                                              serviceSpecificationFind,
                                              serviceSpecificationGet,
                                              serviceSpecificationUpdate)



# @pytest.mark.parametrize("resp, valid, expected_status", [
#     ({}, False, 409),
#     (None, True, 201),
#     (None, False, 403)
# ])
# def test_serviceSpecificationCreate(mocker,
#                                     resp,
#                                     valid,
#                                     expected_status):
#     """test
#     """
#     ServiceSpecification = {
#         "description": "vBox Service for Orange Spain",
#         "name": "vBox_FixedAccess",
#         "version": "1.0"}

#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=resp)

#     bad_rfs_message = {}

#     func_where_is_func_to_mock = 'service_specification_controller'
#     func_to_mock = 'check_cfs_spec'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=[valid,
#                                bad_rfs_message])


#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one_and_update'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock))


#     func_where_is_func_to_mock = 'service_specification_controller'
#     func_to_mock = 'generate_tosca'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock))

#     response, status_code = serviceSpecificationCreate(ServiceSpecification)
#     assert status_code == expected_status


# def test_serviceSpecificationDelete(mocker):
#     """test
#     """
#     ServiceSpecificationId = "0000"

#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'delete_one'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=[{}, 204])
#     response = serviceSpecificationDelete(ServiceSpecificationId)
#     assert response[0] == {}
#     assert response[1] == 204


# def test_serviceSpecificationFind(mocker):
#     """test
#     """

#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=[])

#     response = serviceSpecificationFind()
#     assert response[0] == []
#     assert response[1] == 200


# @pytest.mark.parametrize("resp, expected_status", [
#     ({"key": "value"}, 200),
#     (None, 404)
# ])
# def test_serviceSpecificationGet(mocker, resp, expected_status):
#     """test
#     """
#     ServiceSpecificationId = "0000"
#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=resp)
#     response = serviceSpecificationGet(ServiceSpecificationId)
#     assert response[0] == resp
#     assert response[1] == expected_status


# def test_serviceSpecificationUpdate_ok(mocker):
#     """test
#     """
#     ServiceSpecificationId = "0000"
#     ServiceSpecification = {
#         "description": "vBox Service for Orange Spain",
#         "name": "vBox_FixedAccess",
#         "version": "1.0",
#         "id": "0000"}
#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one_and_update'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=ServiceSpecification)
#     response = serviceSpecificationUpdate(ServiceSpecificationId,
#                                           ServiceSpecification)
#     assert response[0] == ServiceSpecification
#     assert response[1] == 200


# def test_serviceSpecificationUpdate_nok(mocker):
#     """test
#     """
#     ServiceSpecificationId = "0000"
#     ServiceSpecification = {
#         "description": "vBox Service for Orange Spain",
#         "name": "vBox_FixedAccess",
#         "version": "1.0",
#         "id": "0000"}
#     func_where_is_func_to_mock = 'pymongo.collection.Collection'
#     func_to_mock = 'find_one_and_update'
#     mocker.patch((func_where_is_func_to_mock + "." + func_to_mock),
#                  return_value=None)
#     response = serviceSpecificationUpdate(ServiceSpecificationId,
#                                           ServiceSpecification)
#     assert response[0] == None
#     assert response[1] == 404
