"""cfsSpecification related operations
"""
import uuid
import logging
import json
# import os
# import json
# import jsonschema
import cfs_spec_exceptions as service_resolver_exceptions
from cfs_spec_handling import check_cfs_spec, generate_tosca
from cfs_spec_mongo_client import (mongo_find,
                                   mongo_find_one,
                                   mongo_find_one_and_update,
                                   mongo_delete)
from cfs_spec_utils import get_config

LOGGER = logging.getLogger('cfs_spec_api')
SIMUL = get_config("simulator_mode")


def serviceSpecificationCreate(ServiceSpecification):  # pylint: disable=C0103
    """create Service Specification
    """
    LOGGER.debug('call to serviceSpecificationCreate function')
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(ServiceSpecification, indent=4, sort_keys=True))
    LOGGER.debug('data type :  %s', type(ServiceSpecification))

    service_spec = ServiceSpecification
    service_spec["@type"] = "CfsSpecification"
    service_spec["@baseType"] = "ServiceSpecification"
    service_spec["@schemaLocation"] = ""
    service_spec["category"] = "cfs"
    # test_data_path = os.path.dirname(os.path.abspath(__file__))
    # test_data_path = "src/templates/"
    # input_filename = "spec_schema.json"
    # with open(test_data_path + input_filename) as file:
    #     try:
    #         schema = json.load(file)
    #         jsonschema.validate(service_spec, schema)
    #     except Exception as valid_err:
    #         message = "Validation KO: {}".format(valid_err)
    #         LOGGER.error(message)
    #         return message, 409
    # check if proposed cfs Spec composition is valid
    # based on existing services in ONAP SDC
    query = {'name': service_spec["name"]}
    try:
        resp = mongo_find_one(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is not None:
        message_value = ("cfs_spec : " +
                         service_spec["name"] +
                         " already exists")
        message = {"message": message_value}
        LOGGER.debug('message : %s ', message_value)
        return message, 409
    try:
        service_spec = check_cfs_spec(service_spec)
    except (service_resolver_exceptions.RfsSpecException,
            service_resolver_exceptions.RfsSpecNotFoundException,
            service_resolver_exceptions.RfsSpecNotDistributedException,
            service_resolver_exceptions.IncorrectRuleParamsException,
            service_resolver_exceptions.IncorrectTargetValueException,
            service_resolver_exceptions.IncorrectRuleParamsException) as error:
        LOGGER.error("problem : %s", error.args[0])
        message = {"problem": error.args[0]}
        return message, 503
    service_spec['id'] = str(uuid.uuid4())
    service_spec["href"] = "/cfsSpecification/" + service_spec['id']
    query = {'id': service_spec['id']}
    try:
        resp = mongo_find_one_and_update(query, service_spec, True)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if not SIMUL:
        # generate tosca file
        generate_tosca(service_spec)
    return service_spec, 201


def serviceSpecificationDelete(ServiceSpecificationId):  # pylint:disable=C0103
    """delete Service Specification
    """
    LOGGER.debug('call to serviceSpecificationDelete function')
    LOGGER.debug('data received :  %s', ServiceSpecificationId)

    query = {'id': ServiceSpecificationId}
    try:
        resp = mongo_delete(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return resp, 404
    return {}, 204


def serviceSpecificationFind():  # pylint: disable=C0103
    """find ServiceSpecification
    """
    LOGGER.debug('call to serviceSpecificationFind function')

    query = {}
    try:
        resp = mongo_find(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    return resp, 200


def serviceSpecificationGet(ServiceSpecificationId):  # pylint: disable=C0103
    """get ServiceSpecification
    """
    LOGGER.debug('call to serviceSpecificationGet function')
    LOGGER.debug('data received :  %s', ServiceSpecificationId)

    query = {'id': ServiceSpecificationId}
    try:
        resp = mongo_find_one(query)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return {}, 404
    return resp, 200


def serviceSpecificationUpdate(ServiceSpecificationId,  # pylint: disable=C0103
                               ServiceSpecification):
    """update Service Specification
    """
    LOGGER.debug('call to serviceSpecificationUpdate function')
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(ServiceSpecification, indent=4, sort_keys=True))
    LOGGER.debug('data type :  %s', type(ServiceSpecification))
    LOGGER.debug('data received :  %s', ServiceSpecificationId)
    LOGGER.debug('data type :  %s', type(ServiceSpecificationId))

    query = {'id': ServiceSpecificationId}
    try:
        resp = mongo_find_one_and_update(query, ServiceSpecification, False)
    except service_resolver_exceptions.MongodbException:
        LOGGER.error("Unable to connect to Mongodb")
        return {"error": "Unable to connect to Mongodb"}, 503
    if resp is None:
        LOGGER.debug('response is 404 not found')
        return {}, 404
    return resp, 200
