#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""bss-gateway cfsCatalog api
"""

from logging.config import dictConfig
import os
import connexion
from cfs_spec_logdict import conf_dict
from cfs_spec_utils import get_config

# logging setup
CONF = conf_dict('cfs_spec_api.log')
dictConfig(CONF)

CFS_SPEC_API_SERVER = get_config("service_resolver.cfs_spec_api.server")
CFS_SPEC_API_HOST = os.environ['service_spec_server_hostname']
CFS_SPEC_API_PORT = os.environ['service_spec_server_port']
CFS_SPEC_API_DEBUG_MODE = get_config("service_resolver.cfs_spec_api.api_debug_mode")

API_CFS_CATALOG_SERVER = connexion.App(__name__,
                                       specification_dir='swagger/')
API_CFS_CATALOG_SERVER.add_api('swagger.yaml',
                               strict_validation=True,
                               validate_responses=True)
API_CFS_CATALOG_SERVER.run(server=CFS_SPEC_API_SERVER,
                           host=CFS_SPEC_API_HOST,
                           port=CFS_SPEC_API_PORT,
                           debug=CFS_SPEC_API_DEBUG_MODE)
