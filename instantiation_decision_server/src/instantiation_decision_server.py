#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""bss-gateway CFS_ORDER api
"""
from logging.config import dictConfig
import os
import connexion
from instantiation_decision_logdict import conf_dict
from instantiation_decision_utils import get_config

# Logger setup
CONF = conf_dict('service_order.log')
dictConfig(CONF)

INSTANTIATION_DECISION_API_SERVER = get_config("service_resolver.instantiation_decision_api.server")
INSTANTIATION_DECISION_API_HOST = os.environ['instantiation_decision_server_hostname']
INSTANTIATION_DECISION_API_PORT = os.environ['instantiation_decision_server_port']
INSTANTIATION_DECISION_API_DEBUG_MODE = get_config("service_resolver.instantiation_decision_api.api_debug_mode")

API_INSTANTIATION_DECISION_SERVER = connexion.App(__name__,
                                                  specification_dir='swagger/')
API_INSTANTIATION_DECISION_SERVER.add_api('swagger.yaml',
                                          strict_validation=True,
                                          validate_responses=True)
API_INSTANTIATION_DECISION_SERVER.run(server=INSTANTIATION_DECISION_API_SERVER,
                                      host=INSTANTIATION_DECISION_API_HOST,
                                      port=INSTANTIATION_DECISION_API_PORT,
                                      debug=False)
