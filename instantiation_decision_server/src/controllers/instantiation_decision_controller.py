#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
"""instantiation decision related operations
"""
import logging
import json
from decision_operations import perform_decision
import instantiation_decision_exceptions as service_resolver_exceptions

LOGGER = logging.getLogger('instantiation_decision_api')
if LOGGER.hasHandlers():
    # Logger is already configured, remove all handlers
    LOGGER.handlers = []


def instantiationDecisionCreate(DecisionRequestInfo):  # pylint: disable=C0103
    """process a RFS instantiation decision
    """
    inst_decision_info = DecisionRequestInfo
    LOGGER.debug('call to instantiationDecisionCreate function')
    LOGGER.debug('\ndata received (converted into Json) :\n%s\n',
                 json.dumps(inst_decision_info, indent=4, sort_keys=True))
    LOGGER.debug('data type :  %s', type(inst_decision_info))

    try:
        decision = perform_decision(inst_decision_info["orderItem"],
                                    inst_decision_info["rule_params"],
                                    inst_decision_info["rule_results"],
                                    inst_decision_info["rule_name"])
    except (service_resolver_exceptions.ServiceException,
            service_resolver_exceptions.ServiceNotFoundException) as error:
        LOGGER.error("problem : %s", error.args[0])
        message = {"problem": error.args[0]}
        return message, 503
    return decision, 201
