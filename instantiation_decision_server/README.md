# Instantiation Decision API server

## Overview

This API server is about processing an instantiation decision.

## Requirements

## configuration file

Configuration file is config_default.yaml

located in instantiation_decision_server/src/app_conf

It contains mainly information about

* loglevel
* simulator mode
* proxy to get access to ONAP
* mongodb API information
* ONAP various API information
* other Service Resolver API information

## Running via python3

To run the server, please execute the following:

```bash
pip3 install -r requirements.txt
cd src
python3 instantiation_decision_server.py
```

## Running with Docker

To run the server on a Docker container, please execute the following:

```bash
# building the image
docker build -t instantiation_decision_server .

# starting up the container
# need /data repository on the hosts
docker run -p 6006:6006 --volume=/data:/data instantiation_decision_server
```

## Use

open your browser to here:

```bash
http://localhost:6006/serviceResolver/instantiationDecisionManagement/api/v1/ui/
```

Your Swagger definition lives here:

```bash
http://localhost:6006/serviceResolver/instantiationDecisionManagement/api/v1/swagger.json
```
