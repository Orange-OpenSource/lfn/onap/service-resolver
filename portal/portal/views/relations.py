#!/usr/bin/env python

# Copyright (c) 2018 Orange and others.
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
# http://www.apache.org/licenses/LICENSE-2.0
# requests.packages.urllib3.disable_warnings()
#  pylint: disable=import-error
#  pylint: disable=R1702
"""
    relations between customer/cfs/rfs instances
"""
import requests
from flask import Blueprint, render_template
import portal.utils.utils as newportal_utils
# requests.packages.urllib3.disable_warnings()

MOD = Blueprint('relations', __name__, url_prefix='/relations',
                static_folder='../static')
SERVICE_API_HOSTNAME = newportal_utils.get_config("service_resolver.service_api.host")
SERVICE_API_PORT = newportal_utils.get_config("service_resolver.service_api.port")
SERVICE_API_URL = newportal_utils.get_config("service_resolver.service_api.url")
HEADERS = newportal_utils.get_config("service_resolver.service_api.headers")
PROXIES = newportal_utils.get_config("general.proxy")


@MOD.route('/relations_display', methods=['GET'])
def relations_display():
    """
    cfs and rfs instance management
    GET = display cfs / rfs instances with related customer
    """
    service_api_url = ("http://" +
                       SERVICE_API_HOSTNAME +
                       ":" +
                       str(SERVICE_API_PORT) +
                       SERVICE_API_URL +
                       "/service")
    try:
        response = requests.request(
            "GET",
            service_api_url,
            headers=HEADERS,
            proxies=PROXIES)
        services = response.json()
        cfs_list = []
        rfs_list = []
        rfs_supporting_rfs_list = []
        supporting_rfs_list_tmp = []
        customer_list = []
        customer_to_cfs_relation = {}
        customer_to_cfs_relations = []
        cfs_to_rfs_relation = {}
        cfs_to_rfs_relations = []
        relies_on_rfs_relations = []
        relies_on_rfs_relation = {}
        for service in services:
            if service["category"] == "cfs":
                cfs_list.append(service)
                customer = service["relatedParty"][0]
                customer_list.append(customer)
                customer_to_cfs_relation["from"] = customer["id"]
                customer_to_cfs_relation["to"] = service["id"]
                customer_to_cfs_relations.append(customer_to_cfs_relation)
                customer_to_cfs_relation = {}
                for elem in service["supportingService"]:
                    cfs_to_rfs_relation["from"] = service["id"]
                    cfs_to_rfs_relation["to"] = elem["id"]
                    cfs_to_rfs_relations.append(cfs_to_rfs_relation)
                    cfs_to_rfs_relation = {}
            else:
                service["name"] = service["name"][:-30]
                if "reliesOnRfs" in service:
                    for elem in service["reliesOnRfs"]:
                        relies_on_rfs_relation["from"] = service["id"]
                        relies_on_rfs_relation["to"] = elem["id"]
                        relies_on_rfs_relations.append(relies_on_rfs_relation)
                        relies_on_rfs_relation = {}
                        supporting_rfs_list_tmp.append(elem)
                rfs_list.append(service)
        cfs_to_rfs_relations_tmp = cfs_to_rfs_relations.copy()
        for elem1 in relies_on_rfs_relations:
            for elem2 in cfs_to_rfs_relations_tmp:
                if elem1["to"] == elem2["to"]:
                    if elem2 in cfs_to_rfs_relations:
                        cfs_to_rfs_relations.remove(elem2)
        for elem1 in supporting_rfs_list_tmp:
            for elem2 in services:
                if elem2["id"] == elem1["id"]:
                    rfs_supporting_rfs_list.append(elem2)
        rfs_list_tmp = rfs_list.copy()
        for elem1 in rfs_supporting_rfs_list:
            for elem2 in rfs_list_tmp:
                if elem2["id"] == elem1["id"]:
                    if elem1 in rfs_list:
                        rfs_list.remove(elem1)
        customer_list = list({v['id']:v for v in customer_list}.values())
        cfs_list = list({v['id']:v for v in cfs_list}.values())
        rfs_list = list({v['id']:v for v in rfs_list}.values())
        rfs_supporting_rfs_list = list({v['id']:v for v in rfs_supporting_rfs_list}.values())
        return render_template(
            'relations/relations.html',
            cfs_list=cfs_list,
            rfs_list=rfs_list,
            rfs_supporting_rfs_list=rfs_supporting_rfs_list,
            customer_list=customer_list,
            customer_to_cfs_relations=customer_to_cfs_relations,
            cfs_to_rfs_relations=cfs_to_rfs_relations,
            relies_on_rfs_relations=relies_on_rfs_relations)
    except requests.exceptions.ConnectionError:
        message = 'Connection Error to Service API : check rebond tunnel'
        return render_template(
            'exception_annexe.html',
            message=message)
