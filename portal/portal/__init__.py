#!/usr/bin/env python3
# coding: utf-8
"""
    programme de gestion des services et VNF sur ONAP
"""
from flask import Flask
from portal.views import home
from portal.views import relations

MY_WEB_SERVER = Flask(__name__)
MY_WEB_SERVER.register_blueprint(home.MOD)
MY_WEB_SERVER.register_blueprint(relations.MOD)
