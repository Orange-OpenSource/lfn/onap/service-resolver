#!/usr/bin/env python3
# coding: utf-8
"""
    programme de gestion des services et VNF sur ONAP
"""
from portal import MY_WEB_SERVER
import os

PORTAL_HOST = os.environ['portal_server_hostname']
PORTAL_PORT = os.environ['portal_server_port']
MY_WEB_SERVER.run(host=PORTAL_HOST, port=PORTAL_PORT, debug=True)
