#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

"""Camunda requests parameters recuperation from config_default.yaml
"""

from processflow_spec_utils import get_config


URL_REQUEST_GET_PROCESS_DEFINITION = ("http://" +
                           str(get_config("camunda.host")) +
                           ":" + str(get_config("camunda.port")) +
                           get_config("camunda.url") +
                           get_config("camunda.get_process_definition"))

URL_REQUEST_GET_TASKS_LINKED = ("http://" +
                           str(get_config("camunda.host")) +
                           ":" + str(get_config("camunda.port")) +
                           get_config("camunda.url") +
                           get_config("camunda.get_tasks_linked"))
