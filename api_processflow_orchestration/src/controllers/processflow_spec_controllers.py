#!/usr/bin/env python3
#
# This program and the accompanying materials
# are made available under the terms of the Apache License, Version 2.0
# which accompanies this distribution, and is available at
#
# http://www.apache.org/licenses/LICENSE-2.0
#

"""ProcessFlow Spec related operations
"""

import json
import requests
from processflow_spec_client_to_api import (URL_REQUEST_GET_PROCESS_DEFINITION,
                                            URL_REQUEST_GET_TASKS_LINKED)


def processflow_spec_get(name):
    """ to do a GET request of a ProcessFlow definition in Camunda
    """
    new_processflow = {"id": 0, "name": 0, "href": 0,
                       "definition": 0, "version": 0,
                       "taskFlowSpec": []}
    url = URL_REQUEST_GET_PROCESS_DEFINITION

    payload = {}
    headers = {}
    camunda_data = requests.request("GET",
                                    url,
                                    headers=headers,
                                    data=json.dumps(payload))
    resp = None
    if camunda_data is not None:
        resp = camunda_data.json()

        processFlowSpecification_id = None
        for i in resp:
            if i["key"] == name:
                processFlowSpecification_id = i["id"]

    if processFlowSpecification_id is not None:
        url = (URL_REQUEST_GET_PROCESS_DEFINITION +
               "/" + processFlowSpecification_id)
        payload = {}
        headers = {}
        camunda_data = requests.request("GET",
                                        url,
                                        headers=headers,
                                        data=json.dumps(payload))
        resp = None
        if camunda_data is not None:
            resp = camunda_data.json()
            new_processflow["id"] = resp["id"]
            new_processflow["name"] = resp["key"]
            new_processflow["href"] = "/ProcessFlow/" + resp["key"]
            new_processflow["definition"] = resp["description"]
            new_processflow["version"] = resp["id"].split(":")[1]
            if new_processflow["definition"] is None:
                new_processflow["definition"] = "no definition"

            new_processflow = find_taskflows_from_processflow(processFlowSpecification_id, new_processflow)

        return new_processflow, 200

    else:

        return ({"code": 404, "reason": "Not found"}), 404


def processflow_spec_list():
    """ to do a GET request of every ProcessFlow definition in Camunda
    """

    url = URL_REQUEST_GET_PROCESS_DEFINITION

    payload = {}
    headers = {}
    camunda_data = requests.request("GET",
                                    url,
                                    headers=headers,
                                    data=json.dumps(payload))
    resp = None
    new_data = []
    if camunda_data is not None:
        resp = camunda_data.json()

        keys = []

        for i in resp:
            if i["key"] not in keys:
                new_processflow = {"id": 0, "name": 0, "href": 0,
                                   "definition": 0, "version": 0,
                                   "taskFlowSpec": []}
                new_processflow["id"] = i["id"]
                new_processflow["name"] = i["key"]
                new_processflow["definition"] = i["description"]
                if new_processflow["definition"] is None:
                    new_processflow["definition"] = "no definition"
                new_processflow["version"] = i["id"].split(":")[1]
                new_processflow["href"] = "/ProcessFlow/" + i["key"]

                processFlowSpecification_id = i["id"]
                new_processflow = find_taskflows_from_processflow(processFlowSpecification_id, new_processflow)

                new_data.append(new_processflow)
                keys.append(i["key"])
            else:
                for j in new_data:
                    if j["name"] == i["key"]:
                        j["version"] = i["id"].split(":")[1]
        return new_data

    else:
        return ({"code": 400, "reason": "Camunda request failed"}), 400


def find_taskflows_from_processflow(processFlowSpecification_id, new_processflow):

    url = URL_REQUEST_GET_TASKS_LINKED + processFlowSpecification_id
    payload = {}
    headers = {}
    camunda_data = requests.request("GET",
                                    url,
                                    headers=headers,
                                    data=json.dumps(payload))
    resp = None
    taskflows_id = []
    new_taskflow = {"id": 0, "href": 0, "name": 0, "role": 0}
    if camunda_data is not None:
        resp = camunda_data.json()
        for i in resp:
            if i["taskDefinitionKey"] not in taskflows_id:
                new_taskflow["id"] = i["taskDefinitionKey"]
                new_taskflow["href"] = ("/ProcessFlow/" +
                                        i["processDefinitionId"].split(":")[0] +
                                        "/TaskFlow/" + i["taskDefinitionKey"])
                new_taskflow["role"] = i["description"]
                if new_taskflow["role"] is None:
                    new_taskflow["role"] = "no role definition provided"
                new_taskflow["name"] = i["name"]
                new_processflow["taskFlowSpec"].append(new_taskflow)
                taskflows_id.append(i["taskDefinitionKey"])

    return new_processflow
